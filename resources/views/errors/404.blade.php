@extends('errors.layouts.app')

@section('error-title')
  Not Found
@endsection

@section('error-pages')
<div class="page-error">
  <div class="page-inner">
    <h1>404</h1>
    <div class="page-description">
      Halaman yang anda cari tidak ditemukan.
    </div>
  </div>
</div>
@endsection