<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
  <form class="form-inline mr-auto">
    <ul class="navbar-nav mr-3">
      <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
    </ul>
  </form>
  <ul class="navbar-nav navbar-right">
    <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
      <img alt="image" src="{{ Auth::user()->image === null ? Avatar::create(Auth::user()->name)->toBase64() : asset('storage/images/users/' . Auth::user()->image) }}" class="rounded-circle mr-1">
      <div class="d-sm-none d-lg-inline-block">{{ Auth::user()->name }}</div></a>
      <div class="dropdown-menu dropdown-menu-right">
        <a href="{{ route('profile.index', Auth::user()->id) }}" class="dropdown-item has-icon">
          <i class="far fa-user"></i> Profil Saya
        </a>
        <div class="dropdown-divider"></div>
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit(); " class="dropdown-item has-icon text-danger">
          <i class="fas fa-sign-out-alt"></i> Logout

          <form action="{{ route('logout') }}" method="post" id="logout-form">
            @csrf
          </form>
        </a>
      </div>
    </li>
  </ul>
</nav>