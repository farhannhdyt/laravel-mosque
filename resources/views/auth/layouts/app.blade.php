<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Login | App</title>

  {{-- Styles --}}
  @include('backend.assets.styles')
</head>

<body>
  <div id="app">
    @yield('section-body')
  </div>

  {{-- Scripts --}}
  @include('backend.assets.scripts')
  @stack('scripts')
</body>
</html>
