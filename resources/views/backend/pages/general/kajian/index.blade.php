@extends('backend.layouts.app')

@section('section-header')
    Info Kajian
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('kajian') }}
@endsection

@section('section-body')
    {{-- Flash messages --}}
    @include('backend.components.alerts')
    <div class="card">
        <div class="card-header">
            <a href="{{ route('kajian.create') }}" class="btn btn-sm btn-primary" title="Tambah Kajian">
                <i class="fas fa-plus"></i>
            </a>
            <a href="{{ route('kajian.index') }}" class="btn btn-sm btn-success ml-2" id="refresh-data" title="{{ __('Segarkan') }}">
                <i class="fas fa-sync" id="refresh-animation"></i>
            </a>
        </div>

        <div class="card-body">
            <div class="table table-responsive">
                <table class="table-striped" id="datatable" style="width: 100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ __('Judul') }}</th>
                            <th>{{ __('Dibuat Oleh') }}</th>
                            <th>{{ __('Aksi') }}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('backend/develop/js/dataTablesService.js') }}"></script>
    <script src="{{ asset('backend/develop/js/admin/kajians/index.js') }}"></script>
@endpush