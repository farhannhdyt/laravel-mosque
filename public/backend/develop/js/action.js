$(function () {

  // Show modal action
  $('body').on('click', '#btn-modal-show', function (e) {
    e.preventDefault()

    let me = $(this)
        url = me.attr('href')
        title = me.attr('title')

    // Update title modal
    $('#first-modal-title').html(title)

    me.hasClass('edit') ? $('.btn-save').html('Ubah') : $('.btn-save').html('Simpan')
    me.hasClass('show') ? $('.btn-save').hide() : $('.btn-save').show()
    
    $('#first-modal').modal('show')

    $.ajax({
      url: url,
      dataType: 'html',
      beforeSend: function () {
        $('#first-modal-body').html("<p class='text-center'>Loading...</p>")
      },
      success: function (response) {
        $('#first-modal-body').html(response)

        // Disabled keyboard enter in modal
        if ($('#first-modal .modal-body form').length) {
          $('#first-modal .modal-body form').on('keyup keypress', function (e) {
            let keyCode = e.keyCode || e.which
            if (keyCode === 13) {
              e.preventDefault()

              return false
            }
          })
        }
      }
    })
  })

  $("body").on("click", ".btn-save", function(event) {
    event.preventDefault()

    let whichForm = $('.btn-save').hasClass('is-not-modal-form') ? $('#not-modal-form') : $('#first-modal-body form')

    let form = whichForm,
        url = form.prop("action"),
        method = $("input type[name=_method]").val() == undefined ? "POST" : "PUT";

    Swal.fire({
        title: 'Anda Yakin?',
        text: "Pilih dengan seksama!",
        icon: 'question',
        showCancelButton: true,
        cancelButtonText: 'Tidak!',
        confirmButtonColor: '#5969FF',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: url,
                method: method,
                data: new FormData(form[0]),
                contentType: false,
                processData: false,
                beforeSend: function() {
                    form.find(".form-control").removeClass("is-invalid")
                    form.find(".error-message").remove()
                    $(".btn-save").attr("disabled", true)
                },
                success: function(response) {
                    $(".btn-save").attr("disabled", false)
                    $("#first-modal").modal("hide")
                    toastMessage("success", response.message)
                    $("#datatable").DataTable().ajax.reload()
                    if(response.url) window.location.href = response.url
                    form.trigger('reset')
                },
                error: function(xhr) {
                    $(".btn-save").attr("disabled", false)
                    let error = xhr.responseJSON
                    if(error.message == "The given data was invalid.") {
                        toastMessage("error", "Isi form dengan benar.")
                    } else {
                        toastMessage("error", error.message)
                    }
                    if(!$.isEmptyObject(error)) {
                        $.each(error.errors, (key, value) => {
                            $("#" + key)
                                .closest(".form-group")
                                .append(`<p class='text-danger'><i class='fas fa-exclamation-circle'></i> ${value}</p>`)
                                .find(".form-control")
                                .addClass("is-invalid")
                        })
                    }
                }
            })
        }
    })
  })

  // Refresh action
  $("#refresh-data").on('click', function (e) {
    e.preventDefault()

    $.ajax({
      url: $(this).attr('href'),
      type: 'GET',
      beforeSend: function () {
        $('#refresh-animation').addClass('fa-spin')
      },
      success: function () {
        $('#refresh-animation').removeClass('fa-spin')
        toastMessage('success', 'Berhasil disegarkan!')
        $('#datatable').DataTable().ajax.reload()
      },
      error: function (xhr) {
        let error = xhr.responseJSON

        toastMessage('error', error.message)
      }
    })
  })

  // Delete action
  $('body').on('click', '#btn-destroy', function (e) {
    e.preventDefault()

    let me = $(this)
        url = me.attr('href')
        title = me.attr('title')
        csrf_token = $('meta[name=csrf_token]').attr('content');

    Swal.fire({
      title: 'Anda Yakin?',
      text: 'Pilih dengan seksama!',
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#5969FF",
      confirmButtonText: "Ya!",
      cancelButtonText: "TIdak!",
      cancelButtonColor: "#d33"
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: url,
          method: 'POST',
          data: {
            '_method': 'DELETE',
            '_token': csrf_token
          },
          success: function (response) {
            toastMessage('success', response.message)

            $('#datatable').DataTable().ajax.reload()
          },
          error: function (xhr) {
            let error = xhr.responseJSON

            toastMessage('error', error.message)
          }
        })
      }
    })
  })

})