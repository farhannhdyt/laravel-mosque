$(function () {

  const kajianEndpoints = "/admin/kajian/datatables"
  const datas = [
    { data: "DT_RowIndex", name: "id" },
    { data: "title", name: "title" },
    { data: "created_by", name: "created_by" },
    { data: "action", name: "action", orderable: false, searchable: false }
  ]

  dataTables(kajianEndpoints, datas)

})