@extends('backend.layouts.app')

@section('section-header')
    {{ $user->exists ? 'Ubah ' . $user->name : 'Tambah Pengguna Baru' }}
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('store-user', $user) }}
@endsection

@section('section-body')
    {{-- Flash Messages --}}
    @include('backend.components.alerts')
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <a href="{{ route('user.index') }}" class="btn btn-primary" title="Kembali"><i class="fas fa-home"></i></a>
          </div>

          <div class="card-body">
            {!! Form::model($user, [
              'route' => $user->exists ? ['user.update', $user->id] : 'user.store',
              'method' => $user->exists ? 'PUT' : 'POST'
            ]) !!}

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('name', 'Nama') !!}
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ $user->exists ? $user->name : old('name') }}">
                  
                    @error('name')
                      <div class="invalid-feedback">
                        <p><i class="fas fa-exclamation-circle"></i> {{ $message }}</p>
                      </div>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('email', 'Email') !!}
                    <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ $user->exists ? $user->email : old('email') }}">
                  
                    @error('email')
                      <div class="invalid-feedback">
                        <p><i class="fas fa-exclamation-circle"></i> {{ $message }}</p>
                      </div>
                    @enderror
                  </div>
                </div>

                @if(!$user->exists)
                  <div class="col-md-6">
                    <div class="form-group">
                      {!! Form::label('password', 'Password') !!}
                      <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="">
                    
                      @error('password')
                        <div class="invalid-feedback">
                          <p><i class="fas fa-exclamation-circle"></i> {{ $message }}</p>
                        </div>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      {!! Form::label('confirm_password', 'Konfirmasi Password') !!}
                      <input type="password" name="confirm_password" class="form-control @error('confirm_password') is-invalid @enderror" id="">
                    
                      @error('confirm_password')
                        <div class="invalid-feedback">
                          <p><i class="fas fa-exclamation-circle"></i> {{ $message }}</p>
                        </div>
                      @enderror
                    </div>
                  </div>
                @endif
              </div>

              <div class="form-group">
                {!! Form::label('role_id', 'Hak Akses') !!}
                <select name="role_id" class="form-control @error('role_id') is-invalid @enderror">
                  <option value="">-- Pilih Hak Akses --</option>
                  @foreach ($roles as $role)
                      @if($user->role_id === $role->id)
                        <option value="{{ $role->id }}" selected>{{ strtoupper($role->name) }}</option>
                      @endif
                      <option value="{{ $role->id }}">{{ strtoupper($role->name) }}</option>
                  @endforeach
                </select>

                @error('role_id')
                  <div class="invalid-feedback">
                    <p><i class="fas fa-exclamation-circle"></i> {{ $message }}</p>
                  </div>
                @enderror
              </div>

              {!! Form::submit('Simpan', ['class' => 'btn btn-success']) !!}

            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
@endsection