<?php

namespace App\Models\Admin;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Kajian extends Model
{
    /**
     * Get formatted created at
     */
    public function getFormattedCreatedAt() {
        return Carbon::parse($this->created_at)->diffForHumans();
    }
}
