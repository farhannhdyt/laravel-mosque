@extends('frontend.layouts.app')

@section('section-hero')
  <!-- ======= Breadcrumbs ======= -->
  <section class="breadcrumbs">
    <div class="container">

      <div class="d-flex justify-content-between align-items-center">
        <h2>Kajian</h2>
        <ol>
          <li><a href="{{ route('home') }}">Beranda</a></li>
          <li>Kajian</li>
        </ol>
      </div>

    </div>
  </section><!-- End Breadcrumbs -->
@endsection

@section('content-wrapper')
  <section class="inner-page">
    <div class="container">
      <div class="section-title">
        <span>Kajian</span>
        <h2>Kajian</h2>
      </div>

      <div class="row">
        @if($kajian->isEmpty())
          <div class="no-data-wrapper">
            <img src="{{ asset('frontend/images/no_data.svg') }}" alt="" srcset="">
            <p>Saat ini belum ada info kajian</p>
          </div>
        @endif
        @foreach ($kajian as $item)
          <div class="col-md-4 col-sm-12 col-lg-4">
            <div class="card card-kajian">
              <img 
                src="{{ $item->image === null ? asset('backend/images/kajians/noimage.png') : asset('storage/images/kajians/' . $item->image) }}" 
                alt="{{ $item->title }}"
              >
              <div class="card-body">
                <a href="{{ route('kajian.detail', $item->id) }}" class="title">
                  {{ $item->title }}
                </a>
                <span class="speaker">Pemateri : {{ $item->speaker }}</span>
              </div>

              <div class="card-footer bg-white">
                <span class="created">Ditulis Oleh {{ $item->created_by }}</span>
                <span class="created">{{ $item->getFormattedCreatedAt() }}</span> 
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
@endsection