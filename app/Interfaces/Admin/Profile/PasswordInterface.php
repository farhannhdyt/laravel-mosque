<?php

namespace App\Interfaces\Admin\Profile;

use App\Http\Requests\Admin\PasswordRequest;

interface PasswordInterface {

    /**
     * Get user based on id
     * 
     * @param int $id
     */
    public function getUserById($id);

    /**
     * Change Password Method
     * 
     * @param PasswordRequest $request
     * @param int $id
     */
    public function update(PasswordRequest $request, $id);

}