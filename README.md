# Laravel Mosque

Aplikasi pengelolaan data masjid berbasis web menggunakan framework <a href="https://laravel.com/">laravel.</a>

## Clone Repository
Clone Repo

```
git clone https://gitlab.com/farhannhdyt/laravel-mosque.git
```

## Install Dependencies
```
composer install
```

## Dump Autoload
```
composer dump-autoload
```

## Environment
copy .env.example to .env, and configure whatever you want.

## Migrate and Seed the Database
```
php artisan migrate --seed
```

## Generate Symbolic Link
```
php artisan storage:link
```

## Generate New Key
```
php artisan key:generate
```

## Breadcrumbs
copy this breadcrumbs template into your directory <code>vendor/davejamesmiller/laravel-breadcrumbs/views/bootstrap4.blade.php</code>

```
@if (count($breadcrumbs))
    <div class="section-header-breadcrumb">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <div class="breadcrumb-item"><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></div>
            @else
                <div class="breadcrumb-item active">{{ $breadcrumb->title }}</div>
            @endif
        @endforeach
    </div>
@endif
```
Remove an existing template, and paste in. 

## Run It Up
```
php artisan serve
```

## Users
Email | Password  | Role
----- | --------- | -----
admin@app.com | password  | administrator
petugas@app.com  | password  | petugas