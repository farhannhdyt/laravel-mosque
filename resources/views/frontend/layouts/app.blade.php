<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Masjid Al-Ikhlas</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  {{-- Assets --}}
  @include('frontend.assets.styles')

</head>

<body>

  <!-- ======= Header ======= -->
  @include('frontend.layouts.inc._header')

  {{-- Hero Section --}}
  @yield('section-hero')

  <main id="main">

    @yield('content-wrapper')

  </main><!-- End #main -->

  {{-- Footer --}}
  @include('frontend.layouts.inc._footer')

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  {{-- JavaScripts --}}
  @include('frontend.assets.scripts')

</body>

</html>