const dataTables = (url, columns = [], datas = {}) => {
  return $('#datatable').DataTable({
      processing: true,
      serverSide: true,
      responsive: true,

      ajax: {
          url: url,
          data: datas
      },

      columns: columns,
      order: [[1, 'asc']]
  })
}
