<?php

namespace App\Models\Admin;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    /**
     * Get formatted created_at
     */
    public function getFormattedCreatedAt() {
        return Carbon::parse($this->created_at)->diffForHumans();
    }
}
