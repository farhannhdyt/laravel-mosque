<?php

namespace App\Interfaces\Admin\General;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\GalleryRequest;

interface GalleryInterface {

    /**
     * Request galleries
     * 
     * @param Request $request
     */
    public function requestGalleries(Request $request);

    /**
     * Create a new instance gallery
     */
    public function newGallery();

    /**
     * Get gallery based on id
     * 
     * @param int $id
     */
    public function getGalleryById($id);

    /**
     * Store to the database
     * 
     * @param GalleryRequest $request
     * @param int $id
     */
    public function storeToDB(GalleryRequest $request, $id = null);

    /**
     * Delete from the database
     * 
     * @param int $id
     */
    public function deleteGallery($id);

}