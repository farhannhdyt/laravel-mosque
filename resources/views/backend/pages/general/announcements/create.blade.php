@extends('backend.layouts.app')

@section('section-header')
    {{ $announcement->exists ? 'Ubah ' . $announcement->title : 'Buat Pengumuman Baru' }}
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('store-announcement', $announcement) }}
@endsection

@section('section-body')
    {{-- Flash Messages --}}
    @include('backend.components.alerts')
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <a href="{{ route('announcement.index') }}" class="btn btn-primary" title="Kembali"><i class="fas fa-home"></i></a>
          </div>

          <div class="card-body">
            
            {!! Form::model($announcement, [
              'route' => $announcement->exists ? ['announcement.update', $announcement->id] : 'announcement.store',
              'method' => $announcement->exists ? 'PUT' : 'POST',
            ]) !!}

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('title', 'Judul') !!}
                    <input 
                      type="text" 
                      name="title" 
                      value="{{ $announcement->exists ? $announcement->title : old('title') }}"
                      class="form-control @error('title') is-invalid @enderror"
                    >

                    @error('title')
                      <div class="invalid-feedback">
                        <span><i class="fas fa-exclamation-circle"></i> {{ $message }}</span>
                      </div>
                    @enderror
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('subject', 'Perihal') !!}
                    <input 
                      type="text" 
                      name="subject" 
                      value="{{ $announcement->exists ? $announcement->subject : old('subject') }}"
                      class="form-control @error('subject') is-invalid @enderror"
                    >
                    
                    @error('subject')
                      <div class="invalid-feedback">
                        <span><i class="fas fa-exclamation-circle"></i> {{ $message }}</span>
                      </div>
                    @enderror
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    {!! Form::label('description', 'Deskripsi') !!}
                    <textarea 
                      name="description" 
                      class="form-control summernote @error('description') is-invalid @enderror"
                    >
                      {{ $announcement->exists ? $announcement->description : old('description') }}
                    </textarea>
                  
                    @error('description')
                      <div class="invalid-feedback">
                        <span><i class="fas fa-exclamation-circle"></i> {{ $message }}</span>
                      </div>
                    @enderror
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    {!! Form::label('date_time', 'Tanggal & Waktu') !!}
                    <input 
                      type="text" 
                      name="date_time" 
                      class="form-control datetimepicker @error('date_time') is-invalid @enderror"
                      value="{{ $announcement->exists ? $announcement->date_time : old('date_time') }}"
                      autocomplete="off"
                    >

                    @error('description')
                      <div class="invalid-feedback">
                        <span><i class="fas fa-exclamation-circle"></i> {{ $message }}</span>
                      </div>
                    @enderror
                  </div>
                </div>
              </div>

              <button type="submit" class="btn btn-success">Simpan</button>

            {!! Form::close() !!}

          </div>
        </div>
      </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('backend/develop/js/helper.js') }}"></script>
    <script>
      $(function () {
        dateTimePicker('.datetimepicker')
      })
    </script>
@endpush