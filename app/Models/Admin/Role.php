<?php

namespace App\Models\Admin;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Models\Admin\Traits\RoleRelation;

class Role extends Model
{
    use RoleRelation;

    /**
     * Formatted Created At
     */
    public function getFormattedCreatedAt() {
        return Carbon::parse($this->created_at)->diffForHumans();
    }

    /**
     * Formatted Updated At
     */
    public function getFormattedUpdatedAt() {
        return Carbon::parse($this->updated)->diffForHumans();
    }
}
