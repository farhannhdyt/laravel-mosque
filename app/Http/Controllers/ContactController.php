<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\ContactInterface;
use App\Http\Requests\ContactRequest;

class ContactController extends Controller
{
    protected $contactInterface;

    /**
     * Construct
     */
    public function __construct(ContactInterface $contactInterface) {
        $this->contactInterface = $contactInterface;
    }

    /**
     * Store contact into the database
     * 
     * @param ContactRequest $request
     */
    public function store(ContactRequest $request) {
        return $this->contactInterface->storeToDB($request);
    }
}
