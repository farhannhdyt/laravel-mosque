<?php

namespace App\Interfaces\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\RoleRequest;

interface RoleInterface {

    /**
     * Request all roles with ajax
     * 
     * @param Request $request
     */
    public function requestRoles(Request $request);

    /**
     * Create new instance model
     */
    public function newRole();

    /**
     * Get role based on id
     * 
     * @param int $id
     */
    public function getRoleById($id);

    /**
     * Store data to the database
     * 
     * @param RoleRequest $request
     * @param int $id
     */
    public function storeToDB(RoleRequest $request, $id = null);

    /**
     * Delete role from the database
     * 
     * @param int $id
     */
    public function deleteRole($id);

}