@extends('frontend.layouts.app')

@section('section-hero')
  <!-- ======= Breadcrumbs ======= -->
  <section class="breadcrumbs">
    <div class="container">

      <div class="d-flex justify-content-between align-items-center">
        <h2>Pengumuman</h2>
        <ol>
          <li><a href="{{ route('home') }}">Beranda</a></li>
          <li>Pengumuman</li>
        </ol>
      </div>

    </div>
  </section><!-- End Breadcrumbs -->
@endsection

@section('content-wrapper')
  <section class="inner-page">
    <div class="container">
      <div class="section-title">
        <span>Pengumuman</span>
        <h2>Pengumuman</h2>
        <p>Daftar pengumuman terbaru dibawah ini</p>
      </div>

      <div class="row">
        @if($announcements->isEmpty())
          <div class="no-data-wrapper">
            <img src="{{ asset('frontend/images/no_data.svg') }}" alt="" srcset="">
            <p>Saat ini belum ada pengumuman</p>
          </div>
        @endif
        @foreach ($announcements as $item)
          <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="card card-announc">
              <div class="card-body">
                <a href="{{ route('announc.detail', $item->id) }}" class="title">
                  {{ $item->title }}
                </a>
              </div>
              <div class="card-footer bg-white">
                <span class="created">Ditulis Oleh {{ $item->created_by }}</span>
                <span class="created">{{ $item->getFormattedCreatedAt() }}</span> 
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
@endsection