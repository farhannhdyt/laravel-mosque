$(function () {

  const userEndpoints = '/admin/master/user/datatables'
  const datas = [
    { data: "DT_RowIndex", name: "id" },
    { data: "image", name: "image" },
    { data: "name", name: "name" },
    { data: "email", name: "email" },
    { data: "role_id", name: "role_id" },
    { data: "action", name: "action", sortable: false }
  ]

  dataTables(userEndpoints, datas)
})