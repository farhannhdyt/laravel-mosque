@extends('errors.layouts.app')

@section('error-title')
  Bad Gateway
@endsection

@section('error-pages')
<div class="page-error">
  <div class="page-inner">
    <h1>503</h1>
    <div class="page-description">
      Segera kembali.
    </div>
  </div>
</div>
@endsection