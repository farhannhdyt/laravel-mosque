<?php

namespace App\Repositories\Admin\Master;

use DB;
use DataTables;
use App\Models\Admin\Role;
use Illuminate\Http\Request;
use App\Traits\ResponseTrait;
use App\Http\Requests\Admin\RoleRequest;
use App\Interfaces\Admin\Master\RoleInterface;

class RoleRepository implements RoleInterface {

    use ResponseTrait;

    protected $roleModel;

    public function __construct(Role $role) {
        $this->roleModel = $role;
    }

    /**
     * Request roles with ajax
     * 
     * @param Request $request
     */
    public function requestRoles(Request $request) {
        if ($request->ajax()) {
            $role = $this->roleModel->newQuery();

            return DataTables::of($role)
                ->addColumn('action', function ($role) {
                    return view('backend.components.actions', [
                        'editID' => true,
                        'title' => 'Role',
                        'url_show' => route('role.show', $role->id),
                        'url_edit' => route('role.edit', $role->id),
                        'url_destroy' => route('role.destroy', $role->id),
                    ]);
                })
                ->addIndexColumn()
                ->editColumn('name', function ($user) {
                    return strtoupper($user->name);
                })
                ->rawColumns(['action'])
                ->make(true);
        } else {
            return response()->json(['message' => 'This resource accept ajax only'], 500);
        }
    }

    /**
     * Create new instance model
     */
    public function newRole() {
        return new $this->roleModel;
    }

    /**
     * Get role based on id
     * 
     * @param int $id
     */
    public function getRoleById($id) {
        return $this->roleModel->findOrFail($id);
    }

    /**
     * Store data to the database
     * 
     * @param RoleRequest $request
     * @param int $id
     */
    public function storeToDB(RoleRequest $request, $id = null) {
        DB::beginTransaction();

        try {
            $role = $id !== null ? $this->getRoleById($id) : $this->newRole();
            $role->name = $request->name;
            $role->save();

            DB::commit();

            return $this->success(
                $id !== null ? 'Data successfully updated!' : 'Data successfully created!',
                $role,
                $id !== null ? 200 : 201
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Delete role from the database
     * 
     * @param int $id
     */
    public function deleteRole($id) {
        DB::beginTransaction();

        try {
            $role = $this->getRoleById($id)->delete();
            
            DB::commit();

            return $this->success('Data successfully deleted!', $role);
        } catch(\Exception $e) {
            DB::rollback();

            return $this->error($e->getMessage(), $e->getCode());
        }
    }

}