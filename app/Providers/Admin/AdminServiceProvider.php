<?php

namespace App\Providers\Admin;

use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // User Profile
        $this->app->bind('App\Interfaces\Admin\Profile\ProfileInterface', 
        'App\Repositories\Admin\Profile\ProfileRepository');
        
        // User Change Password
        $this->app->bind('App\Interfaces\Admin\Profile\PasswordInterface', 
        'App\Repositories\Admin\Profile\PasswordRepository');

        // User Management
        $this->app->bind('App\Interfaces\Admin\Master\UserInterface', 
        'App\Repositories\Admin\Master\UserRepository');

        // Role Management
        $this->app->bind('App\Interfaces\Admin\Master\RoleInterface', 
        'App\Repositories\Admin\Master\RoleRepository');

        // Takmir Management
        $this->app->bind('App\Interfaces\Admin\General\TakmirInterface',
        'App\Repositories\Admin\General\TakmirRepository');

        // Announcements Management
        $this->app->bind('App\Interfaces\Admin\General\AnnouncementInterface',
        'App\Repositories\Admin\General\AnnouncementRepository');

        // Kajian Management
        $this->app->bind('App\Interfaces\Admin\General\KajianInterface',
        'App\Repositories\Admin\General\KajianRepository');

        // Gallery Management
        $this->app->bind('App\Interfaces\Admin\General\GalleryInterface',
        'App\Repositories\Admin\General\GalleryRepository'
        );
    }
}
