<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Announcement;

class AnnouncementController extends Controller
{
    public function index() {
        $announcements = Announcement::all();

        return view('frontend.pages.announcement.index', compact('announcements'));
    }

    public function show($id) {
        $announcement = Announcement::findOrFail($id);

        return view('frontend.pages.announcement.show', compact('announcement')); 
    }
}
