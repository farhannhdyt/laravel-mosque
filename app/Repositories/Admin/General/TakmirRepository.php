<?php

namespace App\Repositories\Admin\General;

use DB;
use DataTables;
use App\Helpers\Common;
use App\Models\Admin\Takmir;
use Illuminate\Http\Request;
use App\Traits\ResponseTrait;
use App\Http\Requests\Admin\TakmirRequest;
use App\Interfaces\Admin\General\TakmirInterface;

class TakmirRepository implements TakmirInterface {

    // Response json trait
    use ResponseTrait;
    
    // Takmir Model
    protected $model;

    public function __construct(Takmir $takmir) {
        $this->model = $takmir;
    }

    /**
     * Request takmir with ajax
     * 
     * @param Illuminate\Http\Request $request
     */
    public function takmirDataTables(Request $request) {
        if ($request->ajax()) {
            $takmir = $this->model->newQuery();

            return DataTables::of($takmir)
                ->addColumn('action', function ($takmir) {
                    return view('backend.components.actions', [
                        'editID' => true,
                        'title' => 'Takmir',
                        'url_show' => route('takmir.show', $takmir->id),
                        'url_edit' => route('takmir.edit', $takmir->id),
                        'url_destroy' => route('takmir.destroy', $takmir->id),
                    ]);
                })
                ->addIndexColumn()
                ->editColumn('image', function ($takmir) {
                    if ($takmir->image === null) {
                        return "<img src='" . $takmir->getAvatar() .  "' width='60' class='rounded-circle mb-2 mt-2' />";
                    } else {
                        $url = asset('storage/images/takmirs/' . $takmir->image);
                        return "<img src='" . $url .  "' width='60' class='rounded-circle mb-2 mt-2' />";
                    }
                })
                ->editColumn('position', function ($takmir) {
                    return "<span class='badge badge-success'>" . strtoupper($takmir->position) . "</span>";
                })
                ->rawColumns(['action', 'image', 'position'])
                ->make(true);
        } else {
            return response()->json(['message' => 'This request accept ajax only'], 400);
        }
    }

    /**
     * Create a new takmir instance
     */
    public function newTakmir() {
        return new $this->model;
    }

    /**
     * Get takmir based on id
     * 
     * @param int $id
     */
    public function getTakmirById($id) {
        return $this->model->findOrFail($id);
    }

    /**
     * Store data to the database
     * 
     * @param TakmirRequest $request
     * @param int $id
     */
    public function storeToDB(TakmirRequest $request, $id = null) {
        DB::beginTransaction();

        try {
            // if user sends a request with an id then, use method getTakmirById() to handle edit/update action.
            // else, use method newTakmir() to handle create/store action.
            $takmir = $id !== null ? $this->getTakmirById($id) : $this->newTakmir();
            $takmir->image = Common::uploadImage($request, 'image', 'takmirs', $takmir);
            $takmir->name = $request->input('name');
            $takmir->phone = $request->input('phone');
            $takmir->position = $request->input('position');
            $takmir->address = $request->input('address');
            $takmir->save();

            DB::commit();

            return $this->success(
                $id !== null ? 'Takmir berhasil diubah' : 'Takmir berhasil ditambah',
                $takmir,
                $id !== null ? 200 : 201
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Delete takmir from the database
     * 
     * @param int $id
     */
    public function deleteTakmir($id) {
        DB::beginTransaction();

        try {
            $takmir = $this->getTakmirById($id)->delete();

            DB::commit();

            return $this->success(
                'Takmir berhasil dihapus',
                $takmir
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->error($e->getMessage(), $e->getCode());
        }
    }

}