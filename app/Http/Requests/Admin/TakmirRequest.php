<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TakmirRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Customize validation message
     * 
     * @return array
     */
    public function messages() {
        return [
            'image.max' => 'Maksimal gambar 1mb',
            'name.required' => 'Nama wajib diisi',
            'phone.max' => 'Maksimal no telepon 12 karakter',
            'phone.required' => 'No telepon wajib diisi',
            'phone.unique' => 'No telepon sudah terpakai',
            'position.required' => 'Jabatan wajib diisi',
            'address.required' => 'Alamat wajib diisi'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->isMethod('PUT')) {
            return [
                'image' => 'nullable|image|max:1000',
                'name' => 'required',
                'phone' => 'required|max:12|unique:takmirs,phone,' . request()->route('takmir'),
                'position' => 'required',
                'address' => 'required'
            ];
        } else {
            return [
                'image' => 'nullable|image|max:1000',
                'name' => 'required',
                'phone' => 'required|max:12|unique:takmirs,phone',
                'position' => 'required',
                'address' => 'required'
            ];
        }
    }
}
