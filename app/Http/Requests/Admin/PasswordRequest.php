<?php

namespace App\Http\Requests\Admin;

use App\Rules\MatchOldPassword;
use Illuminate\Foundation\Http\FormRequest;

class PasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Customize validate messages
     * 
     * @return array
     */
    public function messages() {
        return [
            'old_password.required' => 'Password Lama tidak boleh kosong',
            'new_password.required' => 'Password Baru tidak boleh kosong',
            'new_password.min' => 'Password minimal 8 karakter',
            'confirm_password.required' => 'Konfirmasi Password tidak boleh kosong',
            'confirm_password.same' => 'Konfirmasi Password tidak valid' 
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => ['required', new MatchOldPassword],
            'new_password' => 'required|string|min:8',
            'confirm_password' => 'required|same:new_password'
        ];
    }
}
