<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\KajianRequest;
use App\Interfaces\Admin\General\KajianInterface;

class KajianController extends Controller
{
    protected $interface;

    public function __construct(KajianInterface $interface) {
        $this->interface = $interface;

        // Only admin and petugas can access this resource
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin_and_petugas')) return $next($request);
            abort(403, config('globalvar.unauthorized'));
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.general.kajian.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kajian = $this->interface->newKajian();

        return view('backend.pages.general.kajian.create', compact('kajian'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KajianRequest $request)
    {
        return $this->interface->storeToDB($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kajian = $this->interface->getKajianById($id);

        return view('backend.pages.general.kajian.show', compact('kajian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kajian = $this->interface->getKajianById($id);

        return view('backend.pages.general.kajian.create', compact('kajian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KajianRequest $request, $id)
    {
        return $this->interface->storeToDB($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->interface->deleteKajian($id);
    }

    /**
     * Get kajian datatables
     * 
     * @param Request $request
     */
    public function kajianDataTables(Request $request) {
        return $this->interface->requestKajian($request);
    }
}
