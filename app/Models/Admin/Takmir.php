<?php

namespace App\Models\Admin;

use Carbon\Carbon;
use Laravolt\Avatar\Avatar;
use Illuminate\Database\Eloquent\Model;

class Takmir extends Model
{
    /**
     * Generate avatar using laravolt avatar packages
     */
    public function getAvatar() {
        $avatar = new Avatar();
        return $avatar->create($this->name)->toBase64();
    }

    /**
     * Get formatted created at
     */
    public function getFormattedCreatedAt() {
        return Carbon::parse($this->created_at)->diffForHumans();
    }

    /**
     * Get formatted updated at
     */
    public function getFormattedUpdatedAt() {
        return Carbon::parse($this->updated_at)->diffForHumans();
    }
}
