<?php

namespace App\Http\Controllers;

use App\Models\Admin\Kajian;
use Illuminate\Http\Request;

class KajianController extends Controller
{
    public function index() {
        $kajian = Kajian::paginate(6);

        return view('frontend.pages.kajian.index', compact('kajian'));
    }

    public function show($id) {
        $kajian = Kajian::findOrFail($id);

        return view('frontend.pages.kajian.show', compact('kajian'));
    } 
}
