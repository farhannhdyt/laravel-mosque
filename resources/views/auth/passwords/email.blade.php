@extends('auth.layouts.app')

@section('section-body')
<section class="section">
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                <div class="login-brand">
                </div>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="card card-primary">
                    <div class="card-header"><h4>{{ __('Lupa Password') }}</h4></div>

                    <div class="card-body">
                        <p class="text-muted">{{ __('Kami akan mengirimkan link untuk reset kata sandi anda') }}</p>
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="form-group">
                                <label for="email">{{ __('Surel') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" tabindex="1" autofocus>
                            
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong><i class="fas fa-exclamation-circle"></i> {{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                    {{ __('Lupa Password') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="simple-footer">
                    &copy; <script>document.writeln(new Date().getFullYear())</script>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection