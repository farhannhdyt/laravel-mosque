<?php

namespace App\Interfaces\Admin\General;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\TakmirRequest;

interface TakmirInterface {
    
    /**
     * Get takmir with datatables
     * 
     * @param Illuminate\Http\Request $request
     */
    public function takmirDataTables(Request $request);
    
    /**
     * Create a new takmir instance
     */
    public function newTakmir();

    /**
     * Get takmir based on id
     * 
     * @param int $id
     */
    public function getTakmirById($id);

    /**
     * Store data to the database
     * 
     * @param TakmirRequest $request
     * @param int $id
     */
    public function storeToDB(TakmirRequest $request, $id = null);

}