<?php

namespace App\Interfaces\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\UserRequest;

interface UserInterface {

    /**
     * Request all user with ajax
     * 
     * @param Request $request
     */
    public function requestUsers(Request $request);

    /**
     * Create new instance model
     */
    public function newUser();

    /**
     * Get user based on id
     * 
     * @param int $id
     */
    public function getUserById($id);
    
    /**
     * Store to the database
     * 
     * @param UserRequest $request
     * @param int $id
     */
    public function storeToDB(UserRequest $request, $id = null);

    /**
     * Delete user from the database
     * 
     * @param int $id
     */
    public function deleteUser($id);

}