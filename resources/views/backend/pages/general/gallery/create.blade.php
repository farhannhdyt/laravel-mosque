{!! Form::model($gallery, [
  'route' => $gallery->exists ? ['gallery.update', $gallery->id] : 'gallery.store',
  'method' => $gallery->exists ? 'PUT' : 'POST',
  'enctype' => 'multipart/form-data'
]) !!}

  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        {!! Form::label('image', 'Gambar (Max: 1mb)') !!}
        {!! Form::file('image', ['class' => 'form-control']) !!}
      </div>
    </div>

    <div class="col-md-12">
      <div class="form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description', $gallery->exists ? $gallery->description : null, ['class' => 'form-control', 'style' => 'height: 100px;']) !!}
      </div>
    </div>
  </div>

{!! Form::close() !!}