<?php

namespace App\Repositories\Admin\Profile;

use DB;
use App\User;
use App\Http\Requests\Admin\PasswordRequest;
use App\Interfaces\Admin\Profile\PasswordInterface;

class PasswordRepository implements PasswordInterface {

    protected $model;

    public function __construct(User $user) {
        $this->model = $user;
    }

    /**
     * Get profile based on id
     * 
     * @param int $id
     */
    public function getUserById($id) {
        return $this->model->findOrFail($id);
    }

    /**
     * Change Password Method
     * 
     * @param PasswordRequest $request
     * @param int $id
     */
    public function update(PasswordRequest $request, $id) {
        DB::beginTransaction();

        try {
            $user = $this->getUserById($id);
            $user->password = \Hash::make($request->new_password);
            $user->save();

            DB::commit();

            return redirect()->back()->with('success', 'Password successfully changed!');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

}