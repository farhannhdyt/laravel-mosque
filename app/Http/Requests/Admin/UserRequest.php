<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Customize validation messages
     * 
     * @return array
     */
    public function messages() {
        return [
            'name.required' => 'Nama wajib diisi',
            'email.required' => 'Surel wajib diisi',
            'role_id.required' => 'Hak Akses wajib diisi',
            'password.required' => 'Password wajib diisi',
            'password.min' => 'Password minimal 8 karakter',
            'confirm_password.required' => 'Konfirmasi Password wajib diisi',
            'confirm_password.same' => 'Konfirmasi Password tidak valid'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->isMethod('PUT')) {
            return [
                "name" => "required",
                "email" => "required|unique:users,email," . request()->route('user'),
                "role_id" => "required"
            ];
        } else {
            return [
                "name" => "required",
                "email" => "required|unique:users,email",
                "password" => "required|min:8",
                "confirm_password" => "required|same:password",
                "role_id" => "required"
            ];
        }
    }
}
