<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class KajianRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Customize validate message
     * 
     * @return array
     */
    public function messages() {
        return [
            'image.max' => 'Maksimal ukuran gambar 1mb',
            'title.required' => 'Judul wajib diisi',
            'description.required' => 'Deskripsi wajib diisi',
            'speaker.required' => 'Pemateri wajib diisi',
            'date.required' => 'Tanggal wajib diisi',
            'time.required' => 'Waktu wajib diisi',
            'location.required' => 'Lokasi wajib diisi'
        ];
        
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'nullable|image|max:1000',
            'title' => 'required',
            'description' => 'required',
            'speaker' => 'required',
            'date' => 'required',
            'time' => 'required',
            'location' => 'required'
        ];
    }
}
