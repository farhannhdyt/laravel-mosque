<div class="main-sidebar">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="{{ route('admin') }}">PROJECT</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="index.html">PR</a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">Dashboard</li>
        <li class="nav-item {{ Request::is('admin') ? 'active' : null }}">
          <a href="{{ route('admin') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
        </li>

        @can('is_admin')
          <li class="menu-header">Master</li>
          <li class="nav-item {{ Request::is('admin/master/user*') ? 'active' : null }}">
            <a href="{{ route('user.index') }}" class="nav-link"><i class="fas fa-users"></i><span>Pengguna</span></a>
          </li>
          <li class="nav-item {{ Request::is('admin/master/role*') ? 'active' : null }}">
            <a href="{{ route('role.index') }}" class="nav-link"><i class="fas fa-code-branch"></i><span>Hak Akses</span></a>
          </li>
        @endcan

        <li class="menu-header">General</li>
        <li class="nav-item {{ Request::is('admin/takmir*') ? 'active' : null }}">
          <a href="{{ route('takmir.index') }}" class="nav-link">
            <i class="fas fa-user-friends"></i>
            <span>Takmir</span>
          </a>
        </li>
        <li class="nav-item {{ Request::is('admin/announcement*') ? 'active' : null }}">
          <a href="{{ route('announcement.index') }}" class="nav-link">
            <i class="fas fa-bullhorn"></i>
            <span>Pengumuman</span>
          </a>
        </li>
        <li class="nav-item {{ Request::is('admin/kajian*') ? 'active' : null }}">
          <a href="{{ route('kajian.index') }}" class="nav-link">
            <i class="fas fa-book-reader"></i>
            <span>Info Kajian</span>
          </a>
        </li>
        <li class="nav-item {{ Request::is('admin/gallery*') ? 'active' : null }}">
          <a href="{{ route('gallery.index') }}" class="nav-link">
            <i class="fas fa-images"></i>
            <span>Galeri</span>
          </a>
        </li>
        <li class="nav-item {{ Request::is('admin/contact*') ? 'active' : null }}">
          <a href="{{ route('contact.index') }}" class="nav-link">
            <i class="fas fa-inbox"></i>
            <span>Pesan</span>
          </a>
        </li>
      </ul>
  </aside>
</div>