@extends('backend.layouts.app')

@section('section-header')
    {{ __('Hak Akses') }}
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('role') }}
@endsection

@section('section-body')
    {{-- Flash Messages --}}
    @include('backend.components.alerts')
    <div class="card">
        <div class="card-header">
            <a href="{{ route('role.create') }}" class="btn btn-sm btn-primary" id="btn-modal-show" title="{{ __('Tambah Hak Akses') }}">
                <i class="fas fa-plus"></i>
            </a>
            <a href="{{ route('role.index') }}" class="btn btn-sm btn-success ml-2" id="refresh-data" title="{{ __('Segarkan') }}">
                <i class="fas fa-sync" id="refresh-animation"></i>
            </a>
        </div>

        <div class="card-body">
            <div class="table table-responsive">
                <table class="table-striped" id="datatable" style="width: 100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ __('Nama') }}</th>
                            <th>{{ __('Aksi') }}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('backend/develop/js/dataTablesService.js') }}"></script>
    <script src="{{ asset('backend/develop/js/admin/roles/index.js') }}"></script>
@endpush