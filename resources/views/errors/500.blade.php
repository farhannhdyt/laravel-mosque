@extends('errors.layouts.app')

@section('error-title')
  Internal Server Error
@endsection

@section('error-pages')
<div class="page-error">
  <div class="page-inner">
    <h1>503</h1>
    <div class="page-description">
      Mohon maaf, ada sedikit gangguan.
    </div>
  </div>
</div>
@endsection