{{-- Core Javascripts --}}
<script src="{{ asset('backend/core/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('backend/core/js/popper.min.js') }}"></script>
<script src="{{ asset('backend/core/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('backend/core/js/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('backend/core/js/moment.min.js') }}"></script>
<script src="{{ asset('backend/core/js/stisla.js') }}"></script>
<script src="{{ asset('backend/core/js/scripts.js') }}"></script>
<script src="{{ asset('backend/core/js/custom.js') }}"></script>

{{-- Plugins --}}
<script src="{{ asset('backend/plugins/sweetalert/sweetalert2@9.js') }}"></script>
<script src="{{ asset('backend/plugins/sweetalert/sweetAlertOption.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('backend/plugins/summernote/summernote-bs4.js') }}"></script>
<script src="{{ asset('backend/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

{{-- Develop --}}
<script src="{{ asset('backend/develop/js/auth.js') }}"></script>
<script src="{{ asset('backend/develop/js/action.js') }}"></script>