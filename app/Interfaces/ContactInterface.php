<?php

namespace App\Interfaces;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;

interface ContactInterface {

    /**
     * Get contact datatables
     * 
     * @param Request
     */
    public function requestContacts(Request $request);

    /**
     * Create a new instance contact
     */
    public function newContact();

    /**
     * Get contact based on id
     * 
     * @param int $id
     */
    public function getContactById($id);

    /**
     * Store contact into the database
     * 
     * @param ContactRequest $request
     */
    public function storeToDB(ContactRequest $request);

    /**
     * Delete contact from the database
     * 
     * @param int $id
     */
    public function deleteContact($id);

}