$(function () {

  const contactEndpoints = "/admin/contact/datatables"
  const datas = [
    { data: "DT_RowIndex", name: "id" },
    { data: "name", name: "name" },
    { data: "email", name: "email" },
    { data: "action", name: "action", orderable: false, searchable: false },
  ]

  dataTables(contactEndpoints, datas)

})