<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\TakmirRequest;
use App\Interfaces\Admin\General\TakmirInterface;

class TakmirController extends Controller
{
    // Bring the interface
    protected $interface;

    public function __construct(TakmirInterface $interface) {
        $this->interface = $interface;

        // Only admin and petugas can access this resource
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin_and_petugas')) return $next($request);
            abort(403, config('globalvar.unauthorized'));
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.general.takmirs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $takmir = $this->interface->newTakmir();
        $position = [
            'penasehat',
            'ketua',
            'wakil ketua',
            'sekertaris',
            'bendahara'
        ];
        return view('backend.pages.general.takmirs.create', compact('takmir', 'position'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TakmirRequest $request)
    {
        return $this->interface->storeToDB($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $takmir = $this->interface->getTakmirById($id);

        return view('backend.pages.general.takmirs.show', compact('takmir'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $takmir = $this->interface->getTakmirById($id);
        $position = [
            'penasehat',
            'ketua',
            'wakil ketua',
            'sekertaris',
            'bendahara'
        ];
        return view('backend.pages.general.takmirs.create', compact('takmir', 'position'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TakmirRequest $request, $id)
    {
        return $this->interface->storeToDB($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->interface->deleteTakmir($id);
    }

    /**
     * Get takmir datatable
     */
    public function takmirDataTables(Request $request) {
        return $this->interface->takmirDataTables($request);
    }
}
