@extends('backend.layouts.app')

@section('section-header')
    Profile {{ $profile->name }}
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('profile', $profile) }}
@endsection

@section('section-body')
    {{-- Flash messages --}}
    @include('backend.components.alerts')

    {{-- Profile Information --}}
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-md-5 col-sm-12 mb-2">
                <img 
                  src="{{ $profile->image === null ? Avatar::create($profile->name)->toBase64() : asset('storage/images/users/' . $profile->image) }}" 
                  alt="{{ $profile->name }}" 
                  width="200"
                  class="d-block mb-2 rounded">

                @if ($profile->image !== null)
                  <a href="{{ route('image.delete', $profile->id) }}" class="btn btn-success mt-2">Delete Image</a>
                @endif
              </div>
              <div class="col-md-7 col-sm-12">
                <table class="table">
                  <tr>
                    <th>Nama</th>
                    <td>:</td>
                    <td>{{ $profile->name }}</td>
                  </tr>
                  <tr>
                    <th>Email</th>
                    <td>:</td>
                    <td>{{ $profile->email }}</td>
                  </tr>
                  <tr>
                    <th>Hak Akses</th>
                    <td>:</td>
                    <td><span class="badge badge-success">{{ strtoupper($profile->getUserRole()) }}</span></td>
                  </tr>
                  <tr>
                    <th>Dibuat Tanggal</th>
                    <td>:</td>
                    <td>
                      {{ $profile->created_at }}
                      ({{ $profile->getFormattedCreatedAt() }})
                    </td>
                  </tr>
                  <tr>
                    <th>Diubah Tanggal</th>
                    <td>:</td>
                    <td>
                      {{ $profile->updated_at }}
                      ({{ $profile->getFormattedUpdatedAt() }})
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      {{-- Edit Profile Section --}}
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h4>Ubah Profil Anda</h4>
          </div>
          <div class="card-body">
            {!! Form::open([
              'route' => ['profile.update', $profile->id],
              'method' => 'PUT',
              'enctype' => 'multipart/form-data'
            ]) !!}

              <div class="row">
                <div class="col-md-12">
                  {!! Form::label('image', 'Gambar', ['style' => 'display: block']) !!}
                  {!! Form::file('image', ['class' => 'form-control mb-3']) !!}
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('name', 'Nama') !!}
                    {!! Form::text('name', $profile->name, ['class' => 'form-control']) !!}
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('email', 'Email') !!}
                    {!! Form::text('email', $profile->email, ['class' => 'form-control']) !!}
                  </div>
                </div>
              </div>

              {!! Form::submit('Simpan Perubahan', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
            
          </div>
        </div>
      </div>

      {{-- Change Password Action --}}
      <div class="col-md-12 change-password-section">
        <div class="card">
          <div class="card-header">
            <h4>Ubah Kata Sandi</h4>
          </div>
          <div class="card-body">
            <form method="post" action="{{ route('password.change', $profile->id) }}">
              @csrf
              @method('PUT')

              <div class="form-group">
                <label for="old_password">Password Lama</label>
                <input type="password" name="old_password" id="oldPassword" class="form-control @error('old_password') is-invalid @enderror">
              
                @error('old_password')
                  <div class="invalid-feedback">
                      <p><i class="fas fa-exclamation-circle"></i> {{ $message }}</p>
                  </div>
                @enderror
              </div>
              <div class="form-group">
                <label for="new_password">Password Baru</label>
                <input type="password" name="new_password" id="newPassword" class="form-control @error('new_password') is-invalid @enderror">
              
                @error('new_password')
                  <div class="invalid-feedback">
                      <p><i class="fas fa-exclamation-circle"></i> {{ $message }}</p>
                  </div>
                @enderror
              </div>
              <div class="form-group">
                <label for="confirm_password">Konfirmasi Password</label>
                <input type="password" name="confirm_password" id="confirmPassword" class="form-control @error('confirm_password') is-invalid @enderror">
              
                @error('confirm_password')
                  <div class="invalid-feedback">
                      <p><i class="fas fa-exclamation-circle"></i> {{ $message }}</p>
                  </div>
                @enderror
              </div>

              <button type="submit" class="btn btn-primary">Ubah Password</button>
            </form>
          </div>
        </div>
      </div>

      {{-- Delete Account Section --}}
      <div class="col-md-12 delete-account-section">
        <div class="card">
          <div class="card-header">
            <h4 class="text-danger">Hapus Akun</h4>
          </div>
          <div class="card-body">
            <p>Setelah Anda menghapus akun Anda, tidak ada jalan untuk kembali. Harap pastikan.</p>
            <a href="{{ route('profile.delete', $profile->id) }}" onclick="event.preventDefault(); document.getElementById('delete-form').submit();" class="btn btn-danger">Hapus Akun</a>
          
            <form action="{{ route('profile.delete', $profile->id) }}" method="post" id="delete-form">
              @csrf
              @method('DELETE')
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection