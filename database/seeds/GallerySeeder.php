<?php

use App\Models\Admin\Gallery;
use Illuminate\Database\Seeder;

class GallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Gallery::create([
            'image' => 'bantuan.jpg',
            'description' => 'Bantuan',
            'created_by' => 'Administrator'
        ]);

        Gallery::create([
            'image' => 'bantuan.jpg',
            'description' => 'Bantuan',
            'created_by' => 'Administrator'
        ]);
        
        Gallery::create([
            'image' => 'bantuan.jpg',
            'description' => 'Kerja Bakti',
            'created_by' => 'Administrator'
        ]);
    }
}
