<?php

namespace App\Http\Controllers\Admin\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PasswordRequest;
use App\Interfaces\Admin\Profile\PasswordInterface;

class PasswordController extends Controller
{
    protected $passInter;

    public function __construct(PasswordInterface $passInter) {
        $this->passInter = $passInter;
    }

    public function index($id) {
        $user = $this->passInter->getUserById($id);

        if ($user->id !== auth()->user()->id) {
            abort(403, "Un-Authorized");
        } else {
            return view('backend.pages.profiles.password', compact('user'));
        }
    }

    public function update(PasswordRequest $request, $id) {
        return $this->passInter->update($request, $id);
    }
}
