@extends('frontend.layouts.app')

@section('section-hero')
  <!-- ======= Breadcrumbs ======= -->
  <section class="breadcrumbs">
    <div class="container">

      <div class="d-flex justify-content-between align-items-center">
        <h2>{{ $announcement->title }}</h2>
        <ol>
          <li><a href="{{ route('home') }}">Beranda</a></li>
          <li><a href="{{ route('announcement') }}">Pengumuman</a></li>
          <li>{{ $announcement->title }}</li>
        </ol>
      </div>

    </div>
  </section><!-- End Breadcrumbs -->
@endsection

@section('content-wrapper')
  <section class="inner-page">
    <div class="container">
      <div class="section-title">
        <h2>{{ $announcement->title }}</h2>
        <p>{{ $announcement->created_by }} - {{ $announcement->getFormattedCreatedAt() }}</p>
      </div>

      <div class="section-body mt-5 announcement-desc">
        {!! $announcement->description !!}
      </div>
    </div>
  </section>
@endsection