<?php

namespace App\Repositories;

use DB;
use DataTables;
use App\Helpers\Common;
use App\Models\Contact;
use Illuminate\Http\Request;
use App\Traits\ResponseTrait;
use App\Interfaces\ContactInterface;
use App\Http\Requests\ContactRequest;

class ContactRepository implements ContactInterface {

    use ResponseTrait;

    protected $model;

    /**
     * Construct
     */
    public function __construct(Contact $model) {
        $this->model = $model;
    }

    /**
     * Get contact datatables
     * 
     * @param Request
     */
    public function requestContacts(Request $request) {
        if ($request->ajax()) {
            $contact = $this->model->newQuery();

            return DataTables::of($contact)
                ->addColumn('action', function ($contact) {
                    return view('backend.components.actions', [
                        'editID' => false,
                        'title' => 'Pesan',
                        'url_destroy' => route('contact.destroy', $contact->id),
                        'url_edit' => '#',
                        'url_show' => route('contact.show', $contact->id),
                    ]);
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        } else {
            return response()->json(['message' => 'This resource accept ajax only'], 500); 
        }
    }

    /**
     * Create a new instance contact
     */
    public function newContact() {
        return new $this->model;
    }

    /**
     * Get contact based on id
     * 
     * @param int $id
     */
    public function getContactById($id) {
        return $this->model->findOrFail($id);
    }

    /**
     * Store contact into the database
     * 
     * @param ContactRequest $request
     */
    public function storeToDB(ContactRequest $request) {
        DB::beginTransaction();

        try {
            $contact = $this->newContact();
            $contact->name = $request->name;
            $contact->email = Common::removeWhiteSpace(strtolower($request->email));
            $contact->subject = $request->subject;
            $contact->body = $request->body;
            $contact->save();

            DB::commit();

            return redirect()->back()->with('success', 'Pesan berhasil dikirim');
        } catch(\Exception $e) {
            DB::rollback();

            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Delete contact from the database
     * 
     * @param int $id
     */
    public function deleteContact($id) {
        DB::beginTransaction();

        try {
            $contact = $this->getContactById($id)->delete();

            DB::commit();

            return $this->success('Pesan berhasil dihapus', $contact);
        } catch(\Exception $e) {
            DB::rollback();

            return $this->error($e->getMessage(), $e->getCode());
        }
    }

}