<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  {{-- CSRF TOKEN --}}
  <meta name="csrf_token" content="{{ csrf_token() }}">
  <title>Laravel Projects</title>
  
  {{-- Styles --}}
  @include('backend.assets.styles')
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      {{-- Navbar --}}
      @include('backend.layouts.include._navbar')

      {{-- Sidebar --}}
      @include('backend.layouts.include._sidebar')

      {{-- Modal --}}
      @include('backend.components.modal')

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>@yield('section-header')</h1>

            @yield('breadcrumbs')
          </div>

          <div class="section-body">
            @yield('section-body')
          </div>
        </section>
      </div>
      
      {{-- Footer --}}
      @include('backend.layouts.include._footer')
    </div>
  </div>

  {{-- Scripts --}}
  @include('backend.assets.scripts')
  @stack('scripts')
</body>
</html>
