<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\ContactInterface;

class ContactController extends Controller
{
    protected $contactInterface;

    /**
     * Construct
     */
    public function __construct(ContactInterface $contactInterface) {
        $this->contactInterface = $contactInterface;
    }
    
    /**
     * Redirecting to contact index
     */
    public function index() {
        return view('backend.pages.general.contacts.index');
    }

    /**
     * Detail of contact
     * 
     * @param int $id
     */
    public function show($id) {
        $contact = $this->contactInterface->getContactById($id);

        return view('backend.pages.general.contacts.show', compact('contact'));
    }

    /**
     * Delete contact from the database
     * 
     * @param int $id
     */
    public function destroy($id) {
        return $this->contactInterface->deleteContact($id);
    }

    /**
     * Get contact datatables
     * 
     * @param Request
     */
    public function contactDataTables(Request $request) {
        return $this->contactInterface->requestContacts($request);
    }
}
