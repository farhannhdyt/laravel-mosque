<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class GalleryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Customize validate message
     * 
     * @return array
     */
    public function messages() {
        return [
            'image.required' => 'Gambar tidak boleh kosong',
            'image.max' => 'Maksimal gambar berukuran 1mb',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->isMethod('PUT')) {
            return [
                'image' => 'nullable|image|max:1000',
                'description' => 'nullable'
            ];
        } else {
            return [
                'image' => 'required|image|max:1000',
                'description' => 'nullable'
            ];
        }
    }
}
