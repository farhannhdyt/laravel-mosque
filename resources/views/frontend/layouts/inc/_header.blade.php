<header id="header" class="fixed-top">
  <div class="container d-flex align-items-center">

    <h1 class="logo mr-auto"><a href="{{ route('home') }}">Al-Ikhlas</a></h1>
    <!-- Uncomment below if you prefer to use an image logo -->
    <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

    <nav class="nav-menu d-none d-lg-block">
      <ul>
        <li class="{{ Request::is('/') ? 'active' : null }}"><a href="{{ route('home') }}">Beranda</a></li>
        <li><a href="#about">Tentang</a></li>
        <li><a href="#services">Pelayanan</a></li>
        <li><a href="#portfolio">Galeri</a></li>
        <li><a href="#team">Takmir</a></li>
        <li class="{{ Request::is('kajian*') ? 'active' : null }}"><a href="{{ route('kajian') }}">Kajian</a></li>
        <li class="{{ Request::is('announcement*') ? 'active' : null }}"><a href="{{ route('announcement') }}">Pengumuman</a></li>
        <li><a href="#contact">Kontak</a></li>

      </ul>
    </nav><!-- .nav-menu -->

    <a href="{{ route('donate') }}" class="get-started-btn scrollto">Bantu Kami</a>

  </div>
</header><!-- End Header -->