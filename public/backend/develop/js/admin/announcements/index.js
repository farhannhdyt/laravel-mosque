$(function () {

  const announcEndpoints = "/admin/announcement/datatables"
  const datas = [
    { data: "DT_RowIndex", name: "id" },
    { data: "title", name: "title" },
    { data: "subject", name: "subject" },
    { data: "created_by", name: "created_by" },
    { data: "action", name: "action", searchable: false, orderable: false }
  ]

  dataTables(announcEndpoints, datas)

})