<div class="row mt-2 mb-2">
  <div class="col-md-6">
    <img src="{{ asset('storage/images/galleries/' . $gallery->image) }}" class="rounded" width="300" alt="{{ $gallery->image }}">
  </div>

  <div class="col-md-6">
    <table class="table table-striped">
      <tr>
        <th>Deskripsi</th>
        <td>:</td>
        <td>
          {{ $gallery->description }}
        </td>
      </tr>

      <tr>
        <th>Dibuat Oleh</th>
        <td>:</td>
        <td>
          {{ $gallery->created_by }}
        </td>
      </tr>
    </table>
  </div>
</div>