<?php

use Illuminate\Support\Facades\Route;

/**
 * ----------------------------------------------
 * Admin Routes
 * ----------------------------------------------
 * 
 * Here is where you can register your admin panel routes.
 */
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'BackendController@dashboard')->name('admin');
    
    // Profiles
    Route::namespace('Profile')->prefix('profile')->group(function() {
        Route::get('/{id}', 'ProfileController@index')->name('profile.index');
        // Update profile
        Route::put('/{id}/update', 'ProfileController@update')->name('profile.update');
        // Remove Image
        Route::get('/{id}/image/deleted', 'ProfileController@deleteImage')->name('image.delete');
        // Delete Profile
        Route::delete('/{id}/profile/delete', 'ProfileController@delete')->name('profile.delete');
    
        // Change Password
        Route::put('/{id}/password/update', 'PasswordController@update')->name('password.change');
    });
    
    // Master Route
    Route::namespace('Master')->prefix('master')->group(function () {
        Route::get('/user/datatables', 'UserController@userDataTables');
        Route::resource('user', 'UserController');
        Route::get('/role/datatables', 'RoleController@roleDataTables');
        Route::resource('role', 'RoleController');
    });
    
    // General Route
    Route::get('/takmir/datatables', 'TakmirController@takmirDataTables');
    Route::resource('takmir', 'TakmirController');
    Route::get('/announcement/datatables', 'AnnouncementController@announcementsDataTables');
    Route::resource('announcement', 'AnnouncementController');
    Route::get('/kajian/datatables', 'KajianController@kajianDataTables');
    Route::resource('kajian', 'KajianController');
    Route::get('/gallery/datatables', 'GalleryController@galleryDataTables');
    Route::resource('gallery', 'GalleryController');
    Route::get('/contact/datatables', 'ContactController@contactDataTables');
    Route::resource('contact', 'ContactController');
});