<?php

namespace App\Http\Controllers\Admin\Master;

use App\Models\Admin\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\UserRequest;
use App\Interfaces\Admin\Master\UserInterface;

class UserController extends Controller
{
    protected $userInterface;
    protected $roleModel;

    public function __construct(UserInterface $userInterface, Role $role) {
        $this->userInterface = $userInterface;
        $this->roleModel = $role;

        // Only admin can access this resource
        $this->middleware(function($request, $next) {
            if(Gate::allows('is_admin')) return $next($request);
            abort(403, config('globalvar.admin_gate_message'));
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.master.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = $this->userInterface->newUser();
        $roles = $this->roleModel->all();

        return view('backend.pages.master.users.create', compact('user', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        return $this->userInterface->storeToDB($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->userInterface->getUserById($id);

        return view('backend.pages.master.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->userInterface->getUserById($id);
        $roles = $this->roleModel->all();

        return view('backend.pages.master.users.create', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        return $this->userInterface->storeToDB($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->userInterface->deleteUser($id);
    }

    /**
     * Get user datatables
     * 
     * @param Request $request
     */
    public function userDataTables(Request $request) 
    {
        return $this->userInterface->requestUsers($request);
    }
}
