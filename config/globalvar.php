<?php

return [
  'admin_gate_message' => 'Hanya admin yang dapat mengakses halaman ini',
  'unauthorized' => 'Anda tidak memiliki izin untuk mengakses halaman ini'
];