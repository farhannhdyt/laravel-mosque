const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 4000,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})

const toastMessage = (icon, title) => {
	/* 
		icon: success, error, warning, info and question.
	*/
	return Toast.fire({
	  icon: icon,
	  title: title
	})
}