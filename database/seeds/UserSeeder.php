<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "Administrator",
            "email" => "admin@app.com",
            "password" => \Hash::make("password"),
            "role_id" => 1
        ]);

        User::create([
            "name" => "Petugas",
            "email" => "petugas@app.com",
            "password" => \Hash::make("password"),
            "role_id" => 2
        ]);
    }
}
