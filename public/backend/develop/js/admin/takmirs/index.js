$(function () {

    const takmirEndpoints = "/admin/takmir/datatables"
    const datas = [
        { data: "DT_RowIndex", name: "id" },
        { data: "image", name: "image" },
        { data: "name", name: "name" },
        { data: "position", name: "position" },
        { data: "action", name: "action", sortable: false }
    ]

    dataTables(takmirEndpoints, datas)

})