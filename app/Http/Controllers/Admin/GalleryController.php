<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\GalleryRequest;
use App\Interfaces\Admin\General\GalleryInterface;

class GalleryController extends Controller
{
    protected $interface;

    /**
     * Construct
     * 
     * @param GalleryInterface $interface
     */
    public function __construct(GalleryInterface $interface) {
        $this->interface = $interface;

        // Only admin and petugas can access this resource
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin_and_petugas')) return $next($request);
            abort(403, config('globalvar.unauthorized'));
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.general.gallery.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gallery = $this->interface->newGallery();

        return view('backend.pages.general.gallery.create', compact('gallery'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GalleryRequest $request)
    {
        return $this->interface->storeToDB($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gallery = $this->interface->getGalleryById($id);

        return view('backend.pages.general.gallery.show', compact('gallery'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery = $this->interface->getGalleryById($id);

        return view('backend.pages.general.gallery.create', compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GalleryRequest $request, $id)
    {
        return $this->interface->storeToDB($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->interface->deleteGallery($id);
    }

    /**
     * Get gallery datatables
     * 
     * @param Request $request
     */
    public function galleryDataTables(Request $request) {
        return $this->interface->requestGalleries($request);
    }
}
