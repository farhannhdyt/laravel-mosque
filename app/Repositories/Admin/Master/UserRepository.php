<?php

namespace App\Repositories\Admin\Master;

use DB;
use App\User;
use DataTables;
use App\Helpers\Common;
use Laravolt\Avatar\Avatar;
use Illuminate\Http\Request;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Admin\UserRequest;
use App\Interfaces\Admin\Master\UserInterface;

class UserRepository implements UserInterface {

    use ResponseTrait;
    
    protected $userModel;

    public function __construct(User $userModel) {
        $this->userModel = $userModel;
    }

    /**
     * Request all users with ajax
     * 
     * @param Request $request
     */
    public function requestUsers(Request $request) {
        if ($request->ajax()) {
            $user = $this->userModel->newQuery();

            return DataTables::of($user)
                ->addColumn('action', function ($user) {
                    return view('backend.components.actions', [
                        'editID' => false,
                        'title' => 'User',
                        'url_show' => route('user.show', $user->id),
                        'url_edit' => route('user.edit', $user->id),
                        'url_destroy' => route('user.destroy', $user->id),
                    ]);
                })
                ->addIndexColumn()
                ->editColumn('image', function ($user) {
                    if ($user->image === null) {
                        $avatar = $user->getAvatar();
                        return "<img src='" . $avatar .  "' width='60' class='rounded-circle mb-2 mt-2' />";
                    } else {
                        $url = asset('storage/images/users/' . $user->image);
                        return "<img src='" . $url .  "' width='60' class='rounded-circle mb-2 mt-2' />";
                    }
                })
                ->editColumn('role_id', function ($user) {
                    return "<span class='badge badge-success'>" . strtoupper($user->getUserRole()) . "</span>";
                })
                ->rawColumns(['action', 'image', 'role_id'])
                ->make(true);
        } else {
            return response()->json(['message' => 'This resource accept ajax only'], 500);
        }
    }

    /**
     * New instance model
     */

    public function newUser() {
        return new $this->userModel;
    }

    /**
     * Get user based on id
     * 
     * @param int $id
     */
    public function getUserById($id) {
        return $this->userModel->findOrFail($id);
    }

    /**
     * Store to the database
     * 
     * @param UserRequest $request
     * @param int $id
     */
    public function storeToDB(UserRequest $request, $id = null) {
        DB::beginTransaction();

        try {
            $user = $id !== null ? $this->getUserById($id) : $this->newUser();
            $user->name = $request->name;
            $user->email = Common::removeWhiteSpace(strtolower($request->email));
            
            if ($id === null) {
                $user->password = \Hash::make($request->password);
            }
            $user->role_id = $request->role_id;

            $user->save();

            DB::commit();

            if ($id === null) {
                return redirect()->route('user.index')->with('success', 'Pengguna berhasil ditambah!');
            } else {
                return redirect()->route('user.index')->with('success', 'Pengguna berhasil diubah!');
            }

        } catch (\Exception $e) {
            DB::rollback();

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Delete user from the database
     * 
     * @param int $id
     */
    public function deleteUser($id) {
        DB::beginTransaction();

        try {
            $user = $this->getUserById($id);

            if ($user->id === Auth::user()->id) {
                return response()->json(['message' => 'Anda tidak bisa menghapus disini!'], 500);
            } else {
                $user->delete();
            }

            DB::commit();

            return $this->success('Pengguna berhasil dihapus!', $user);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->error($e->getMessage(), $e->getCode());

        }
    }

}