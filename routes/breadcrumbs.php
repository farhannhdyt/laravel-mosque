<?php

/**
 * -------------------------------------
 * Breadcrumbs Route
 * -------------------------------------
 * 
 * Here is where you can register your breadcrumbs routes.
 * This package was created by https://github.com/davejamesmiller/laravel-breadcrumbs
 */

/**
 * Admin Panel Breadcrumbs
 */
Breadcrumbs::for('dashboard', function ($trail) {
    $trail->push('Dashboard', route('admin'));
});

// Profile Breadcrumbs
    Breadcrumbs::for('profile', function ($trail, $profile) {
        $trail->parent('dashboard');
        $trail->push('Profil ' . $profile->name, route('profile.index', $profile->id));
    });
// End Profile Breadcrumbs

// Master Breadcrumbs
    Breadcrumbs::for('user', function ($trail) {
        $trail->parent('dashboard');
        $trail->push('Pengguna', route('user.index'));
    });

    Breadcrumbs::for('store-user', function ($trail, $user) {
        $trail->parent('user');
        if (Request::is('admin/master/user/' . $user->id . '/edit')) {
            $trail->push('Ubah ' . $user->name, route('user.edit', $user->id));
        } else {
            $trail->push('Tambah Pengguna Baru', route('user.create'));
        }
    });
    
    Breadcrumbs::for('role', function ($trail) {
        $trail->parent('dashboard');
        $trail->push('Hak Akses', route('role.index'));
    });
// End Master Breadcrumbs

// General Breadcrumbs
    Breadcrumbs::for('takmir', function ($trail) {
        $trail->parent('dashboard');
        $trail->push('Takmir', route('takmir.index'));
    });

    Breadcrumbs::for('announcement', function ($trail) {
        $trail->parent('dashboard');
        $trail->push('Pengumuman', route('announcement.index'));
    });

    Breadcrumbs::for('store-announcement', function ($trail, $announcement) {
        $trail->parent('announcement');
        if (Request::is('admin/announcement/' . $announcement->id . '/edit')) {
            $trail->push('Ubah ' . $announcement->title, route('announcement.edit', $announcement->id));
        } else {
            $trail->push('Buat Pengumuman Baru', route('announcement.create'));
        }
    });

    Breadcrumbs::for('kajian', function ($trail) {
        $trail->parent('dashboard');
        $trail->push('Info Kajian', route('kajian.index'));
    });

    Breadcrumbs::for('store-kajian', function ($trail, $kajian) {
        $trail->parent('kajian');
        
        if (Request::is('admin/kajian/' . $kajian->id . '/edit')) {
            $trail->push('Ubah Info Kajian', route('kajian.edit', $kajian->id));
        } else {
            $trail->push('Tambah Kajian Baru', route('kajian.create'));
        }
    });

    Breadcrumbs::for('gallery', function ($trail) {
        $trail->parent('dashboard');
        $trail->push('Galeri', route('gallery.index'));
    });

    Breadcrumbs::for('contact', function ($trail) {
        $trail->parent('dashboard');
        $trail->push('Pesan', route('contact.index'));
    });
// End General Breadcrumbs