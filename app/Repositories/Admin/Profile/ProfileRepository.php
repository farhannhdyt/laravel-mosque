<?php

namespace App\Repositories\Admin\Profile;

use DB;
use App\User;
use App\Helpers\Common;
use App\Http\Requests\Admin\ProfileRequest;
use App\Interfaces\Admin\Profile\ProfileInterface;

class ProfileRepository implements ProfileInterface {

    protected $model;

    public function __construct(User $user) {
        $this->model = $user;
    }

    /**
     * Get profile based on id
     * 
     * @param int $id
     */
    public function getProfileById($id) {
        return $this->model->findOrFail($id);
    }

    /**
     * Update profile
     * 
     * @param ProfileRequest $request
     * @param $id
     */
    public function update(ProfileRequest $request, $id) {
        DB::beginTransaction();

        try {
            $profile = $this->getProfileById($id);
            $profile->image = Common::uploadImage($request, 'image', 'users', $profile);
            $profile->name = $request->name;
            $profile->email = Common::removeWhiteSpace(strtolower($request->email));
            $profile->save();

            DB::commit();

            return redirect()->back()->with('success', 'Profil berhasil diubah!');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Delete Image method
     * 
     * @param int $id
     */
    public function deleteImage($id) {
        DB::beginTransaction();

        try {
            $profile = $this->getProfileById($id);
            Common::deleteImage('users', $profile);
            $profile->save();

            DB::commit();

            return redirect()->back()->with('success', 'Gambar berhasil dihapus!');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Delete Profile
     * 
     * @param int $id
     */
    public function delete($id) {
        DB::beginTransaction();

        try {
            $profile = $this->getProfileById($id);
            Common::deleteImage('users', $profile);
            $profile->delete();

            DB::commit();

            return redirect()->route('login')->with('success', 'Akun anda berhasil dihapus permanen!');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

}