$(function () {

  const roleEndpoints = "/admin/master/role/datatables"
  const datas = [
    { data: "DT_RowIndex", name: "id" },
    { data: "name", name: "name" },
    { data: "action", name: "action", sortable: false }
  ]

  dataTables(roleEndpoints, datas)

})