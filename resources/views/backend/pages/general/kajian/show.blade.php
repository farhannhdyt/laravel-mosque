<div class="row">
  <div class="col-md-12">
    <img 
      src="{{ $kajian->image === null ? asset('backend/images/kajians/noimage.png') : asset('storage/images/kajians/' . $kajian->image) }}" 
      style="width: 100%; height: 400px;"
    />
  </div>
</div>

<hr>

<table class="table table-striped">
  <tr>
    <th>Judul</th>
    <td>:</td>
    <td>
      {{ $kajian->title }}
    </td>
  </tr>
  <tr>
    <th>Pemateri</th>
    <td>:</td>
    <td>
      {{ $kajian->speaker }}
    </td>
  </tr>
  <tr>
    <th>Deskripsi</th>
    <td>:</td>
    <td>
      {!! $kajian->description !!}
    </td>
  </tr>
  <tr>
    <th>Tanggal</th>
    <td>:</td>
    <td>
      {!! $kajian->date !!}
    </td>
  </tr>
  <tr>
    <th>Waktu</th>
    <td>:</td>
    <td>
      {!! $kajian->time !!} - Sampai dengan selesai
    </td>
  </tr>
</table>