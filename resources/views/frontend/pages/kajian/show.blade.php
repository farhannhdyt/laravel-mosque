@extends('frontend.layouts.app')

@section('section-hero')
  <!-- ======= Breadcrumbs ======= -->
  <section class="breadcrumbs">
    <div class="container">

      <div class="d-flex justify-content-between align-items-center">
        <h2 style="font-size: 25px">{{ $kajian->title }}</h2>
        <ol>
          <li><a href="{{ route('home') }}">Beranda</a></li>
          <li><a href="{{ route('kajian') }}">Kajian</a></li>
          <li>Info Kajian</li>
        </ol>
      </div>

    </div>
  </section><!-- End Breadcrumbs -->
@endsection

@section('content-wrapper')
  <section class="inner-page">
    <div class="container">
      <div class="section-title">
        <h2>{{ $kajian->title }}</h2>
        <p>{{ $kajian->created_by }} - {{ $kajian->getFormattedCreatedAt() }}</p>
      </div>

      <div class="section-body mt-5 kajian-desc">
        <img 
          src="{{ $kajian->image === null ? asset('backend/images/kajians/noimage.png') : asset('storage/images/kajians/' . $kajian->image) }}" 
          alt="{{ $kajian->title }}"
        >
        {!! $kajian->description !!}
      </div>
    </div>
  </section>
@endsection