<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * Get formatted created at
     */
    public function getFormattedCreatedAt() {
        return Carbon::parse($this->created_at)->diffForHumans();
    }
}
