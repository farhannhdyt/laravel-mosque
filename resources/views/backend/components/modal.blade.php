<div class="modal fade" id="first-modal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="first-modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="first-modal-body">

            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-success pull-left btn-save">
                    <span class="fa fa-refresh fa-spin loading-button"></span>
                    Save
                </button>
            </div>
        </div>
    </div>
</div>
