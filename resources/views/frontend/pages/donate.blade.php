@extends('frontend.layouts.app')

@section('section-hero')
  <!-- ======= Breadcrumbs ======= -->
  <section class="breadcrumbs">
    <div class="container">

      <div class="d-flex justify-content-between align-items-center">
        <h2>Donasi</h2>
        <ol>
          <li><a href="{{ route('home') }}">Beranda</a></li>
          <li>Donasi</li>
        </ol>
      </div>

    </div>
  </section><!-- End Breadcrumbs -->
@endsection

@section('content-wrapper')
  <section class="inner-page">
    <div class="container">
      <div class="section-title">
        <span>Donasi</span>
        <h2>Donasi</h2>
      </div>

      <div class="card border-0">
        <div class="card-body text-center">
          <p>Dari Jabir bin ‘Abdillah radhiyallahu ‘anhu, Rasulullah shallallahu ‘alaihi wa sallam bersabda,</p>
          <span style="font-size: 20px">مَنْ بَنَى مَسْجِدًا لِلَّهِ كَمَفْحَصِ قَطَاةٍ أَوْ أَصْغَرَ بَنَى اللَّهُ لَهُ بَيْتًا فِى الْجَنَّةِ</span>
          <p class="mt-2">
            “Siapa yang membangun masjid karena Allah walaupun hanya selubang tempat burung bertelur atau lebih kecil, maka Allah bangunkan baginya (rumah) seperti itu pula di surga.” (HR. Ibnu Majah no. 738. Al-Hafizh Abu Thahir mengatakan bahwa sanad hadits ini shahih)
          </p>
        </div>
      </div>

      <div class="card border-primary mt-5">
        <div class="card-body">
          <h6 style="font-weight: bold;">Yuk mari berdonasi melalui rekening dibawah</h6>

          <div class="credit-card-wrapper mt-5 table-responsive">
            <table class="table" style="font-size: 15px">
              <tr>
                <th>Bank</th>
                <td>:</td>
                <td>
                  BNI
                </td>
              </tr>
              <tr>
                <th>No Rekening</th>
                <td>:</td>
                <td>
                  8029898012389091
                </td>
              </tr>
              <tr>
                <th>Atas Nama</th>
                <td>:</td>
                <td>
                  Masjid Al-Ikhlas
                </td>
              </tr>
            </table>
          </div>

        </div>
      </div>
      <div class="mt-3">
        <p>
          Kami ucapakan Jazakallahu khoiron (semoga Allah membalasmu dengan kebaikan). Aamiin.
        </p>
      </div>
    </div>
  </section>
@endsection