<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\AnnouncementRequest;
use App\Interfaces\Admin\General\AnnouncementInterface;

class AnnouncementController extends Controller
{
    protected $interface;

    public function __construct(AnnouncementInterface $interface) {
        $this->interface = $interface;

        // Only admin and petugas can access this resource
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin_and_petugas')) return $next($request);
            abort(403, config('globalvar.unauthorized'));
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.general.announcements.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $announcement = $this->interface->newAnnouncement();

        return view('backend.pages.general.announcements.create', compact('announcement'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnnouncementRequest $request)
    {
        return $this->interface->storeToDB($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $announcement = $this->interface->getAnnouncementById($id);
        return view('backend.pages.general.announcements.show', compact('announcement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announcement = $this->interface->getAnnouncementById($id);
        return view('backend.pages.general.announcements.create', compact('announcement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnnouncementRequest $request, $id)
    {
        return $this->interface->storeToDB($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->interface->deleteAnnouncement($id);
    }

    /**
     * Get Announcements DataTables
     */
    public function announcementsDataTables(Request $request) {
        return $this->interface->requestAnnouncements($request);
    }
}
