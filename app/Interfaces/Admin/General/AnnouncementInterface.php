<?php

namespace App\Interfaces\Admin\General;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\AnnouncementRequest;

interface AnnouncementInterface {

    /**
     * Request Announcements With Ajax
     * 
     * @param Illuminate\Http\Request $request
     */
    public function requestAnnouncements(Request $request);

    /**
     * Create a new instance announcement
     */
    public function newAnnouncement();

    /**
     * Get announcement based on id
     * 
     * @param int $id
     */
    public function getAnnouncementById($id);

    /**
     * Store to the database
     * 
     * @param AnnouncementRequest $request
     * @param int $id
     */
    public function storeToDB(AnnouncementRequest $request, $id = null);

    /**
     * Delete data from the database
     * 
     * @param int $id
     */
    public function deleteAnnouncement($id);

}