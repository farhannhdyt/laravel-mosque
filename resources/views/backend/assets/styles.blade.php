{{-- Core Styles --}}
<link rel="stylesheet" href="{{ asset('backend/core/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset('backend/core/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('backend/core/css/components.css') }}">

{{-- Plugins --}}
<link rel="stylesheet" href="{{ asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('backend/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}">