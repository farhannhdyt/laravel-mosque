<a href="{{ $url_show  }}" class="btn btn-sm btn-info show" id="btn-modal-show" title="Detail {{ $title }}"><em class="fa fa-eye"></em></a>
<a href="{{ $url_edit }}" class="btn btn-sm btn-warning edit" id="{{ $editID ? 'btn-modal-show' : null }}" title="Ubah {{ $title }}"><em class="fa fa-edit"></em></a>
<a href="{{ $url_destroy }}" class="btn btn-sm btn-danger" id="btn-destroy" title="Hapus {{ $title }}"><em class="fa fa-trash"></em></a>
