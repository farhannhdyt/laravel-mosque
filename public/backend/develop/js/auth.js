let button = document.getElementById('button-login')

const doSubmit = () => {
  button.setAttribute('disabled', '')
  button.setAttribute('value', 'Signing In...')

  return true
}

button.removeAttribute('disabled')
button.setAttribute('value', 'Sign In')
