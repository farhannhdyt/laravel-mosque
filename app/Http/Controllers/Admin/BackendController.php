<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\Admin\Kajian;
use Illuminate\Http\Request;
use App\Models\Admin\Announcement;
use App\Http\Controllers\Controller;

class BackendController extends Controller
{
    public function dashboard() {
        $total_of_users = User::count();
        $total_of_announc = Announcement::count();
        $total_of_kajian = Kajian::count();

        return view('backend.pages.dashboard', compact(
            'total_of_users', 
            'total_of_announc', 
            'total_of_kajian'
        ));
    }
}
