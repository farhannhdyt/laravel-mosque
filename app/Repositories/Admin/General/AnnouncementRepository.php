<?php

namespace App\Repositories\Admin\General;

use DB;
use DataTables;
use Illuminate\Http\Request;
use App\Traits\ResponseTrait;
use App\Models\Admin\Announcement;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Admin\AnnouncementRequest;
use App\Interfaces\Admin\General\AnnouncementInterface;

class AnnouncementRepository implements AnnouncementInterface {

    use ResponseTrait;
    
    protected $model;

    public function __construct(Announcement $model) {
        $this->model = $model;
    }

    /**
     * Request Announcements With Ajax
     * 
     * @param Illuminate\Http\Request $request
     */
    public function requestAnnouncements(Request $request) {
        // request ajax only
        if ($request->ajax()) {
            $announcement = $this->model->newQuery();

            return DataTables($announcement)
                ->addColumn('action', function ($announcement) {
                    return view('backend.components.actions', [
                        'editID' => false,
                        'title' => 'Pengumuman',
                        'url_destroy' => route('announcement.destroy', $announcement->id),
                        'url_edit' => route('announcement.edit', $announcement->id),
                        'url_show' => route('announcement.show', $announcement->id),
                    ]);
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        } else {
            return response()->json(['message' => 'This resource accept ajax only'], 500);
        }
    }

    /**
     * Create a new instance announcement
     */
    public function newAnnouncement() {
        return new $this->model;
    }

    /**
     * Get announcement based on id
     * 
     * @param int $id
     */
    public function getAnnouncementById($id) {
        return $this->model->findOrFail($id);
    }

    /**
     * Store to the database
     * 
     * @param AnnouncementRequest $request
     * @param int $id
     */
    public function storeToDB(AnnouncementRequest $request, $id = null) {
        DB::beginTransaction();

        try {
            $announcement = $id !== null ? $this->getAnnouncementById($id) : $this->newAnnouncement();
            $announcement->title = $request->input('title');
            $announcement->subject = $request->input('subject');
            $announcement->description = $request->input('description');
            $announcement->date_time = $request->input('date_time');
            $announcement->created_by = Auth::user()->name;
            
            $announcement->save();
            DB::commit();
            
            if ($id !== null) {
                return redirect()->route('announcement.index')->with('success', 'Pengumuman berhasil diubah');
            } else {
                return redirect()->route('announcement.index')->with('success', 'Pengumuman berhasil ditambah');
            }
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Delete data from the database
     * 
     * @param int $id
     */
    public function deleteAnnouncement($id) {
        DB::beginTransaction();

        try {
            $announcement = $this->getAnnouncementById($id)->delete();

            DB::commit();

            return $this->success('Pengumuman berhasil dihapus', $announcement);
        } catch(\Exception $e) {
            DB::rollback();

            return $this->error($e->getMessage(), $e->getCode());
        }
    }
}