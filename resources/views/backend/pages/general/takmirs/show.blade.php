<div class="row">
  <div class="col-md-12 text-center">
    <img 
      src="{{ $takmir->image === null ? Avatar::create($takmir->name)->toBase64() : asset('storage/images/takmirs/' . $takmir->image) }}" 
      alt="{{ $takmir->name }}" 
      class="rounded-circle"
      style="width: 100px;"
    >
  </div>
</div>

<table class="table table-striped">
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td>
      {{ $takmir->name }}
    </td>
  </tr>

  <tr>
    <th>No Telepon</th>
    <td>:</td>
    <td>
      {{ $takmir->phone }}
    </td>
  </tr>

  <tr>
    <th>Jabatan</th>
    <td>:</td>
    <td>
      <span class="badge badge-success">{{ strtoupper($takmir->position) }}</span>
    </td>
  </tr>

  <tr>
    <th>Alamat</th>
    <td>:</td>
    <td>
      {{ $takmir->address }}
    </td>
  </tr>

  <tr>
    <th>Dibuat Tanggal</th>
    <td>:</td>
    <td>
      {{ $takmir->created_at }} 
      ({{ $takmir->getFormattedCreatedAt() }})
    </td>
  </tr>

  <tr>
    <th>Diubah Tanggal</th>
    <td>:</td>
    <td>
      {{ $takmir->updated_at }} 
      ({{ $takmir->getFormattedUpdatedAt() }})
    </td>
  </tr>
</table>