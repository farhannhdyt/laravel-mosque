<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@home')->name('home');
Route::get('/announcement', 'AnnouncementController@index')->name('announcement');
Route::get('/announcement/detail/{id}', 'AnnouncementController@show')->name('announc.detail');
Route::get('/donate', 'FrontController@donate')->name('donate');
Route::get('/kajian', 'KajianController@index')->name('kajian');
Route::get('/kajian/detail/{id}', 'KajianController@show')->name('kajian.detail');
Route::post('/contact', 'ContactController@store')->name('contact.send');

Auth::routes();

Route::get('/home', 'HomeController@index');
