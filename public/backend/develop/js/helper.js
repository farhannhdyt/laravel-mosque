/**
 * Bootstrap Date Time Picker
 * 
 * @param {*} element 
 */

// Date Time Picker
const dateTimePicker = (element) => {
  return $(element).datetimepicker({
    format: 'DD/MM/YYYY HH:mm', 
    useCurrent: false,
    showTodayButton: true,
    showClear: true,
    toolbarPlacement: 'bottom',
    sideBySide: true,
    icons: {
        time: "fas fa-clock",
        date: "fas fa-calendar",
        up: "fas fa-arrow-up",
        down: "fas fa-arrow-down",
        previous: "fas fa-chevron-left",
        next: "fas fa-chevron-right",
        today: "fas fa-clock",
        clear: "fas fa-trash"
    },
    locale: 'id'
  })
}

// Time Only
const timePicker = (element) => {
  return $(element).datetimepicker({
    format: "LT",
    icons: {
      time: "fas fa-clock",
      up: "fas fa-arrow-up",
      down: "fas fa-arrow-down"
    },
    locale: 'id'
  })
}

// Date Only
const datePicker = (element) => {
  return $(element).datetimepicker({
    format: 'DD/MM/YYYY',
    icons: {
      date: "fas fa-calendar",
      up: "fas fa-arrow-up",
      down: "fas fa-arrow-down",
      previous: "fas fa-chevron-left",
      next: "fas fa-chevron-right",
      today: "fas fa-clock",
      clear: "fas fa-trash"
    },
    locale: 'id'
  })
}
