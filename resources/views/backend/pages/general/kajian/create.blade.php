@extends('backend.layouts.app')

@section('section-header')
    {{ $kajian->exists ? $kajian->title : 'Buat Kajian Baru' }}
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('store-kajian', $kajian) }}
@endsection

@section('section-body')
    {{-- Flash Messages --}}
    @include('backend.components.alerts')
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <a href="{{ route('kajian.index') }}" class="btn btn-primary" title="Kembali"><i class="fas fa-home"></i></a>
          </div>

          <div class="card-body">
            {!! Form::model($kajian, [
              'route' => $kajian->exists ? ['kajian.update', $kajian->id] : 'kajian.store',
              'method' => $kajian->exists ? 'PUT' : 'POST',
              'enctype' => 'multipart/form-data'
            ]) !!}

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    {!! Form::label('image', 'Gambar') !!}
                    {!! Form::file('image', ['class' => 'form-control']) !!}
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('title', 'Judul') !!}
                    <input type="text" name="title" value="{{ $kajian->exists ? $kajian->title : old('title') }}" class="form-control @error('title') is-invalid @enderror">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('speaker', 'Pemateri') !!}
                    <input type="text" name="speaker" value="{{ $kajian->exists ? $kajian->speaker : old('speaker') }}" class="form-control @error('speaker') is-invalid @enderror">
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    {!! Form::label('description', 'Deskripsi') !!}
                    <textarea 
                      name="description" 
                      class="form-control summernote @error('description') is-invalid @enderror"
                    >
                      {{ $kajian->exists ? $kajian->description : old('description') }}
                    </textarea>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('date', 'Tanggal') !!}
                    <input type="text" name="date" value="{{ $kajian->exists ? $kajian->date : old('date') }}" class="form-control datepicker @error('date') is-invalid @enderror">
                  </div>
                </div>
                
                <div class="col-md-6">
                  <div class="form-group">
                    {!! Form::label('time', 'Waktu') !!}
                    <input type="text" name="time" value="{{ $kajian->exists ? $kajian->time : old('time') }}" class="form-control timepicker @error('time') is-invalid @enderror">
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    {!! Form::label('location', 'Lokasi') !!}
                    <textarea name="location" class="form-control @error('location') is-invalid @enderror" style="height: 100px">
                      {{ $kajian->exists ? $kajian->location : old('location') }}
                    </textarea>
                  </div>
                </div>
              </div>

              {!! Form::submit('Simpan', ['class' => 'btn btn-success']) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('backend/develop/js/helper.js') }}"></script>
    <script>
      $(function () {
        timePicker('.timepicker')
        datePicker('.datepicker')
      })
    </script>
@endpush