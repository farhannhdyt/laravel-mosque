@extends('backend.layouts.app')

@section('section-header')
    Pesan
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('contact') }}
@endsection

@section('section-body')
    {{-- Flash Messages --}}
    @include('backend.components.alerts')
    <div class="card">
        <div class="card-header">
            <a href="{{ route('contact.index') }}" class="btn btn-sm btn-success ml-2" id="refresh-data" title="{{ __('Segarkan') }}">
                <i class="fas fa-sync" id="refresh-animation"></i>
            </a>
        </div>

        <div class="card-body">
            <div class="table table-responsive">
                <table class="table-striped" id="datatable" style="width: 100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('backend/develop/js/dataTablesService.js') }}"></script>
    <script src="{{ asset('backend/develop/js/admin/contacts/index.js') }}"></script>
@endpush