@extends('frontend.layouts.app')

@section('section-hero')
  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center">
          <h1>Masjid Al-Ikhlas</h1>
          <h2>Selamat datang di website resmi Masjid Jami Al-Ikhlas</h2>
          <div class="d-flex">
            <a href="{{ route('donate') }}" class="btn-get-started scrollto">Bantu Kami</a>
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img">
          <img src="{{ asset('frontend/images/kubah.png') }}" class="rounded img-fluid animated" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->
@endsection

@section('content-wrapper')

  <!-- ======= About Section ======= -->
  <section id="about" class="about">
    <div class="container">

      <div class="row">
        <div class="col-lg-6">
          <img src="{{ asset('frontend/images/mosque.jpg') }}" class="img-fluid rounded" alt="">
        </div>
        <div class="col-lg-6 pt-4 pt-lg-0 content">
          <h3>Sekilas Tentang Masjid Jami Al-Ikhlas</h3>
          <p class="font-italic">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua.
          </p>
          <ul>
            <li><i class="icofont-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat</li>
            <li><i class="icofont-check-circled"></i> Duis aute irure dolor in reprehenderit in voluptate velit</li>
            <li><i class="icofont-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda</li>
          </ul>
          <p>
            Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
            velit esse cillum dolore eu fugiat nulla pariatur.
          </p>
        </div>
      </div>

    </div>
  </section><!-- End About Section -->

  <!-- ======= Services Section ======= -->
  <section id="services" class="services section-bg">
    <div class="container">

      <div class="section-title">
        <span>Pelayanan</span>
        <h2>Pelayanan</h2>
        <p>Kami menyediakan beberapa pelayanan untuk jama'ah di sekitaran masjid ataupun di luar masjid.</p>
      </div>

      <div class="row">
        <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
          <div class="icon-box">
            <div class="icon"><i class="bx bxl-dribbble"></i></div>
            <h4><a href="">Unit Pelayanan Zakat</a></h4>
            <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
          <div class="icon-box">
            <div class="icon"><i class="bx bx-file"></i></div>
            <h4><a href="">E-Perpustakaan</a></h4>
            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
          <div class="icon-box">
            <div class="icon"><i class="bx bx-tachometer"></i></div>
            <h4><a href="">Pusat Informasi</a></h4>
            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
          </div>
        </div>

      </div>

    </div>
  </section><!-- End Services Section -->

  <!-- ======= Portfolio Section ======= -->
  <section id="portfolio" class="portfolio">
    <div class="container">

      <div class="section-title">
        <span>Galeri</span>
        <h2>Galeri</h2>
        <p>Berikut galeri kegiatan kami</p>
      </div>

      <div class="row portfolio-container">

        @foreach($galleries as $gallery)
          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="{{ asset('storage/images/galleries/' . $gallery->image) }}" class="img-fluid rounded" alt="Gallery">
            <div class="portfolio-info">
              <p>{{ $gallery->description }}</p>
              <a href="{{ asset('storage/images/galleries/' . $gallery->image) }}" data-gall="portfolioGallery" class="venobox preview-link" title="{{ $gallery->description }}"><i class="bx bx-plus"></i></a>
            </div>
          </div>
        @endforeach

      </div>

    </div>
  </section><!-- End Portfolio Section -->

  <!-- ======= Team Section ======= -->
  <section id="team" class="team section-bg">
    <div class="container">

      <div class="section-title">
        <span>Takmir</span>
        <h2>Takmir</h2>
        <p>Para takmir pengurus Masjid Jami Al-Ikhlas</p>
      </div>

      <div class="row">
        @foreach($takmir as $item)
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="member">
              <img 
                src="{{ $item->image === null ? Avatar::create($item->name)->toBase64() : asset('storage/images/takmirs/' . $item->image) }}" 
                alt="{{ $item->name }}"
              >
              <h4>{{ $item->name }}</h4>
              <span>{{ $item->position }}</span>
            </div>
          </div>
        @endforeach

      </div>

    </div>
  </section><!-- End Team Section -->

  <!-- ======= Contact Section ======= -->
  <section id="contact" class="contact">
    <div class="container">

      <div class="section-title">
        <span>Kontak</span>
        <h2>Kontak</h2>
        <p>Informasi kontak</p>
      </div>

      <div class="row">

        <div class="col-lg-5 d-flex align-items-stretch">
          <div class="info">
            <div class="address">
              <i class="icofont-google-map"></i>
              <h4>Lokasi:</h4>
              <p>Jl. Antabaru Dalam 4 No.VII Bandung</p>
            </div>

            <div class="email">
              <i class="icofont-envelope"></i>
              <h4>Email:</h4>
              <p>info@example.com</p>
            </div>

            <div class="phone">
              <i class="icofont-phone"></i>
              <h4>No Telepon:</h4>
              <p>0851-2839-4893</p>
            </div>

            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen></iframe>
          </div>

        </div>

        <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
          <form action="{{ route('contact.send') }}" class="php-email-form" method="post">
            @csrf
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="name">Nama</label>
                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" />
                @error('name') 
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
              <div class="form-group col-md-6">
                <label for="name">Email</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" />
                @error('email') 
                  <div class="invalid-feedback">
                    {{ $message }}
                  </div>
                @enderror
              </div>
            </div>
            <div class="form-group">
              <label for="name">Subjek</label>
              <input type="text" class="form-control @error('subject') is-invalid @enderror" name="subject" id="subject" />
              @error('subject') 
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
              @enderror
            </div>
            <div class="form-group">
              <label for="name">Pesan</label>
              <textarea class="form-control @error('body') is-invalid @enderror" name="body" rows="10"></textarea>
              @error('body') 
                <div class="invalid-feedback">
                  {{ $message }}
                </div>
              @enderror
            </div>
            <div class="mb-3">
              @include('backend.components.alerts')
            </div>
            <div class="text-center"><button type="submit">Kirim Pesan</button></div>
          </form>
        </div>

      </div>

    </div>
  </section><!-- End Contact Section -->
@endsection