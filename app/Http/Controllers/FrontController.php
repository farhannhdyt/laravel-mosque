<?php

namespace App\Http\Controllers;

use App\Models\Admin\Takmir;
use Illuminate\Http\Request;
use App\Models\Admin\Gallery;
use App\Models\Admin\Announcement;

class FrontController extends Controller
{
    /**
     * Home Page Controller
     */
    public function home() {
        $galleries = Gallery::paginate(6);
        $takmir = Takmir::all();

        return view('frontend.pages.home', compact('galleries', 'takmir'));
    }

    public function donate() {
        return view('frontend.pages.donate');
    }
}
