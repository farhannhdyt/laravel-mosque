<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Customize Validation Message
     * 
     * @return array
     */
    public function messages() {
        return [
            'title.required' => 'Judul tidak boleh kosong',
            'subject.required' => 'Perihal tidak boleh kosong',
            'description.required' => 'Deskripsi tidak boleh kosong',
            'date_time.required' => 'Tanggal & Waktu tidak boleh kosong'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'subject' => 'required',
            'description' => 'required',
            'date_time' => 'required'
        ];
    }
}
