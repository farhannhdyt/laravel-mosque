@extends('auth.layouts.app')

@section('section-body')
    <section class="section">
        <div class="container mt-5">
            <div class="row">
                <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                    {{-- Flash Message --}}
                    @include('backend.components.alerts')
                    <div class="login-brand">
                    </div>

                    <div class="card card-primary">
                        <div class="card-header"><h4>{{ __('Masuk') }}</h4></div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('login') }}" onsubmit="doSubmit()">
                                @csrf

                                <div class="form-group">
                                    <label for="email">{{ __('Email') }}</label>
                                    <input id="email" type="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="{{ __('emailanda@contoh.com') }}" tabindex="1">
                                    
                                    @error('email')
                                        <div class="invaid-feedback">
                                            <p class="text-danger"><i class="fas fa-exclamation-circle"></i> {{ $message }}</p>
                                        </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <div class="d-block">
                                        <label for="password" class="control-label">{{ __('Kata Sandi') }}</label>
                                        <div class="float-right">
                                            @if (Route::has('password.request'))
                                                <a class="text-small" href="{{ route('password.request') }}">
                                                    {{ __('Lupa Password?') }}
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" tabindex="2">

                                    @error('password')
                                        <div class="invaid-feedback">
                                            <p class="text-danger"><i class="fas fa-exclamation-circle"></i> {{ $message }}</p>
                                        </div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
                                        <label class="custom-control-label" for="remember-me">{{ __('Ingat Saya') }}</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input type="submit" value="Sign In" id="button-login" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                </div>
                            </form> 
                        </div>
                    </div>
                    <div class="simple-footer">
                        &copy; <script>document.writeln(new Date().getFullYear())</script>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection