@extends('errors.layouts.app')

@section('error-title')
  Forbidden
@endsection

@section('error-pages')
<div class="page-error">
  <div class="page-inner">
    <h1>403</h1>
    <div class="page-description">
      Anda tidak diizinkan untuk mengakses halaman ini.
    </div>
  </div>
</div>
@endsection