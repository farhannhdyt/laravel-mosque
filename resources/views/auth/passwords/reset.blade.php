@extends('auth.layouts.app')

@section('section-body')
<section class="section">
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                <div class="login-brand">
                </div>

                <div class="card card-primary">
                    <div class="card-header"><h4>{{ __('Reset Kata Sandi') }}</h4></div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('password.update') }}">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group">
                                <label for="email">{{ __('Surel') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" value="{{ $email ?? old('email')}}" name="email" tabindex="1" autofocus>
                                
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password">{{ __('Kata Sandi Baru') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" tabindex="2">
                            
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password-confirm">{{ __('Konfrimasi Kata Sandi') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" tabindex="2">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                    {{ __('Reset Kata Sandi') }}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="simple-footer">
                    <script>document.writeln(new Date().getFullYear())</script>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection