<table class="table table-striped">
  <tr>
    <th>Nama</th>
    <td>:</td>
    <td>
      {{ $contact->name }}
    </td>
  </tr>
  <tr>
    <th>Email</th>
    <td>:</td>
    <td>
      {{ $contact->email }}
    </td>
  </tr>
  <tr>
    <th>Subjek</th>
    <td>:</td>
    <td>
      {{ $contact->subject }}
    </td>
  </tr>
  <tr>
    <th>Pesan</th>
    <td>:</td>
    <td>
      {{ $contact->body }}
    </td>
  </tr>
  <tr>
    <th>Dibuat</th>
    <td>:</td>
    <td>
      {{ $contact->created_at }} - 
      ({{ $contact->getFormattedCreatedAt() }})
    </td>
  </tr>
</table>