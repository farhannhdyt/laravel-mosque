{!! Form::model($takmir, [
  'route' => $takmir->exists ? ['takmir.update', $takmir->id] : 'takmir.store',
  'method' => $takmir->exists ? 'PUT' : 'POST',
  'enctype' => 'multipart/form-data'
]) !!}

  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        {!! Form::label('image', 'Gambar') !!}
        {!! Form::file('image', ['class' => 'form-control']) !!}
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group">
        {!! Form::label('name', 'Nama') !!}
        {!! Form::text('name', $takmir->exists ? $takmir->name : null, ['class' => 'form-control']) !!}
      </div>
    </div>
    
    <div class="col-md-6">
      <div class="form-group">
        {!! Form::label('phone', 'No Telepon') !!}
        {!! Form::text('phone', $takmir->exists ? $takmir->phone : null, ['class' => 'form-control']) !!}
      </div>
    </div>

    <div class="col-md-12">
      <div class="form-group">
        {!! Form::label('position', 'Jabatan') !!}

        <select name="position" class="form-control" id="">
          <option value="">-- Pilih Jabatan --</option>
          @foreach ($position as $item)
              @if ($takmir->position === $item)
                <option value="{{ $item }}" selected>{{ strtoupper($item) }}</option>
              @endif
              <option value="{{ $item }}">{{ strtoupper($item) }}</option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="col-md-12">
      <div class="form-group">
        {!! Form::label('address', 'Alamat') !!}
        {!! Form::textarea('address', $takmir->exists ? $takmir->address : null, ['class' => 'form-control', 'style' => 'height: 100px']) !!}
      </div>
    </div>
  </div>

{!! Form::close() !!}