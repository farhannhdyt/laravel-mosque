<?php

namespace App\Repositories\Admin\General;

use DB;
use DataTables;
use App\Helpers\Common;
use Illuminate\Http\Request;
use App\Models\Admin\Gallery;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Admin\GalleryRequest;
use App\Interfaces\Admin\General\GalleryInterface;

class GalleryRepository implements GalleryInterface {

    // Response JSON Trait
    use ResponseTrait;
    
    // Gallery Model
    protected $model;

    /**
     * Construct
     * 
     * @param Gallery $model
     */
    public function __construct(Gallery $model) {
        $this->model = $model;
    }

    /**
     * Request gallery
     * 
     * @param Request $request
     */
    public function requestGalleries(Request $request) {
        if ($request->ajax()) {
            $gallery = $this->model->newQuery();

            return DataTables::of($gallery)
                ->addColumn('action', function ($gallery) {
                    return view('backend.components.actions', [
                        'editID' => true,
                        'title' => 'Galeri',
                        'url_destroy' => route('gallery.destroy', $gallery->id),
                        'url_edit' => route('gallery.edit', $gallery->id),
                        'url_show' => route('gallery.show', $gallery->id)
                    ]);
                })
                ->addIndexColumn()
                ->editColumn('image', function ($gallery) {
                    $asset = asset('storage/images/galleries/' . $gallery->image);
                    return "<img src='" . $asset .  "' width='80' class='rounded mb-2 mt-2' />";
                })
                ->rawColumns(['action', 'image'])
                ->make(true);
        } else {
            return response()->json(['message' => 'This resource accept ajax only'], 500);
        }
    }

    /**
     * Create a new instance gallery
     */
    public function newGallery() {
        return new $this->model;
    }

    /**
     * Get gallery based on id
     * 
     * @param int $id
     */
    public function getGalleryById($id) {
        return $this->model->findOrFail($id);
    }
    
    /**
     * Store to the database
     * 
     * @param GalleryRequest $request
     * @param int $id
     */
    public function storeToDB(GalleryRequest $request, $id = null) {
        DB::beginTransaction();

        try {
            $gallery = $id !== null ? $this->getGalleryById($id) : $this->newGallery();
            $gallery->image = Common::uploadImage($request, 'image', 'galleries', $gallery);
            $gallery->description = $request->input('description');
            $gallery->created_by = Auth::user()->name;

            $gallery->save();

            DB::commit();

            return $this->success(
                $id !== null ? 'Galeri berhasil diubah' : 'Galeri berhasil ditambah',
                $gallery,
                $id !== null ? 200 : 201
            );
        } catch (\Exception $e) {
            DB::rollback();

            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Delete from the database
     * 
     * @param int $id
     */
    public function deleteGallery($id) {
        DB::beginTransaction();

        try {
            $gallery = $this->getGalleryById($id)->delete();

            DB::commit();

            return $this->success('Galeri berhasil dihapus', $gallery);
        } catch (\Exception $e) {
            DB::rollback();

            return $this->error($e->getMessage(), $e->getCode());
        } 
    }

}