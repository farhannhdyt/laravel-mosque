<?php

namespace App\Interfaces\Admin\Profile;

use App\Http\Requests\Admin\ProfileRequest;

interface ProfileInterface {

    /**
     * Get profile based on id
     * 
     * @param int $id
     */
    public function getProfileById($id);

    /**
     * Update profile
     * 
     * @param ProfileRequest $request
     * @param $id
     */
    public function update(ProfileRequest $request, $id);

    /**
     * Delete Image method
     * 
     * @param int $id
     */
    public function deleteImage($id);

    /**
     * Delete Profile
     * 
     * @param int $id
     */
    public function delete($id);

}