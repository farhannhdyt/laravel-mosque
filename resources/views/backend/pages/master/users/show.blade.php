<div class="row mb-4">
  <div class="col-md-12 text-center">
    <img src="{{ $user->image === null ? Avatar::create($user->name)->toBase64() : asset('storage/images/users/' . $user->image) }}" alt="{{ $user->name }}" class="rounded-circle" width="120">
  </div>
</div>

<table class="table table-striped">
  <tr>
    <th>{{ __('Nama') }}</th>
    <td>:</td>
    <td>
      {{ $user->name }}
    </td>
  </tr>
  <tr>
    <th>{{ __('Email') }}</th>
    <td>:</td>
    <td>
      {{ $user->email }}
    </td>
  </tr>
  <tr>
    <th>{{ __('Hak Akses') }}</th>
    <td>:</td>
    <td>
      <span class="badge badge-success">{{ strtoupper($user->getUserRole()) }}</span>
    </td>
  </tr>
  <tr>
    <th>{{ __('Dibuat Tanggal') }}</th>
    <td>:</td>
    <td>
      {{ $user->created_at }} 
      ({{ $user->getFormattedCreatedAt() }})
    </td>
  </tr>
  <tr>
    <th>{{ __('Diubah Tanggal') }}</th>
    <td>:</td>
    <td>
      {{ $user->created_at }} 
      ({{ $user->getFormattedUpdatedAt() }})
    </td>
  </tr>
</table>