<?php

namespace App\Http\Controllers\Admin\Profile;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\Admin\ProfileRequest;
use App\Interfaces\Admin\Profile\ProfileInterface;

class ProfileController extends Controller
{
    protected $profileInterface;

    public function __construct(ProfileInterface $profileInterface) {
        $this->profileInterface = $profileInterface;
    }

    public function index($id) {
        $profile = $this->profileInterface->getProfileById($id);
        
        return view('backend.pages.profiles.index', compact('profile'));
    }

    /**
     * Update profile
     * 
     * @param ProfileRequest $request
     * @param $id
     */
    public function update(ProfileRequest $request, $id) {
        return $this->profileInterface->update($request, $id);
    }

    /**
     * Delete Image method
     * 
     * @param int $id
     */
    public function deleteImage($id) {
        return $this->profileInterface->deleteImage($id);
    }

    /**
     * Delete Profile
     * 
     * @param int $id
     */
    public function delete($id) {
        return $this->profileInterface->delete($id);
    }
}
