<table class="table table-striped">
  <tr>
    <th>Name</th>
    <td>:</td>
    <td>
      {{ strtoupper($role->name) }}
    </td>
  </tr>

  <tr>
    <th>Created At</th>
    <td>:</td>
    <td>
      {{ $role->created_at }} 
      ({{ $role->getFormattedCreatedAt() }})
    </td>
  </tr>

  <tr>
    <th>Updated At</th>
    <td>:</td>
    <td>
      {{ $role->updated_at }} 
      ({{ $role->getFormattedUpdatedAt() }})
    </td>
  </tr>
</table>