$(function () {

  const galleryEndpoints = '/admin/gallery/datatables'
  const datas = [
    { data: 'DT_RowIndex', name: 'id' },
    { data: 'image', name: 'image' },
    { data: 'created_by', name: 'created_by' },
    { data: 'action', name: 'action', orderable: false, searchable: false }
  ]

  dataTables(galleryEndpoints, datas)

})