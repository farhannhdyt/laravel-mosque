<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>@yield('error-title')</title>

  @include('backend.assets.styles')
</head>

<body>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        
        @yield('error-pages')

        <div class="simple-footer mt-5">
          Copyright &copy; Masjid Al-Ikhlas <script>document.writeln(new Date().getFullYear())</script>
        </div>
      </div>
    </section>
  </div>

  @include('backend.assets.scripts')
</body>
</html>
