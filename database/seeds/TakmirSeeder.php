<?php

use App\Models\Admin\Takmir;
use Illuminate\Database\Seeder;

class TakmirSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Takmir::create([
            'image' => 'man.png',
            'name' => 'John Doe',
            'phone' => '08812039584',
            'position' => 'penasehat',
            'address' => 'Jl.Antabaru Dalam 3 No.5 Bandung'
        ]);

        Takmir::create([
            'image' => 'man.png',
            'name' => 'Abdul Qadir',
            'phone' => '08592347813',
            'position' => 'ketua',
            'address' => 'Jl.Antabaru 2 No.10 Bandung'
        ]);

        Takmir::create([
            'image' => 'man.png',
            'name' => 'Ahmad',
            'phone' => '09283781349',
            'position' => 'wakil ketua',
            'address' => 'Jl.Antabaru Endah 10 No.5 Bandung'
        ]);
    }
}
