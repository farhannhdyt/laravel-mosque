<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Common {

    /**
     * Remove whitespace in string
     * 
     * @param $string
     * @return string|string[]|null
     */
    public static function removeWhiteSpace($string) {
        return preg_replace('/\s+/', '', $string);
    }

    /**
     * Upload file
     * 
     * @param Request $request
     * @param $requestName
     * @param $storePath
     * @param $model
     * 
     * @return string
     */
    public static function uploadImage(
        Request $request,
        $requestName,
        $storePath,
        $model = null
    ) {
        if ($request->hasFile($requestName)) {
            // Get file name with extension
            $fileNameWithExt = $request->file($requestName)->getClientOriginalName();

            // Get file name
            $fileName = \pathinfo($fileNameWithExt, PATHINFO_FILENAME);

            // Get file Ext
            $fileExtension = $request->file($requestName)->getClientOriginalExtension();

            // File name to store
            $fileNameToStore = self::removeWhiteSpace(strtolower($fileName)) . '-' . rand() . '.' . $fileExtension;

            // Path to store
            $path = $request->file($requestName)->storeAs("public/images/$storePath", $fileNameToStore);

            // if there's a model, delete an existing model
            if ($model) {
                Storage::delete("public/images/$storePath/$model->image");
            }

            return $fileNameToStore;
        } else {
            // if there's no file or user didn't input anything
            if ($model) return $model->image;
            else return $fileNameToStore = null;
        }
    }

    /**
     * Delete file from the storage
     */
    public static function deleteImage($path, $model) {
        return Storage::delete("public/images/$path/" . $model->image);
    }

}