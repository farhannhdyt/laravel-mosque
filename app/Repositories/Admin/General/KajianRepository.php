<?php

namespace App\Repositories\Admin\General;

use DB;
use DataTables;
use App\Helpers\Common;
use App\Models\Admin\Kajian;
use Illuminate\Http\Request;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Admin\KajianRequest;
use App\Interfaces\Admin\General\KajianInterface;

class KajianRepository implements KajianInterface {

    use ResponseTrait;

    protected $model;

    public function __construct(Kajian $kajian) {
        $this->model = $kajian;
    }

    /**
     * Request kajian using ajax
     * 
     * @param Request $request
     */
    public function requestKajian(Request $request) {
        if ($request->ajax()) {
            $kajian = $this->model->newQuery();

            return DataTables::of($kajian)
                ->addColumn('action', function ($kajian) {
                    return view('backend.components.actions', [
                        'editID' => false,
                        'title' => 'Kajian',
                        'url_destroy' => route('kajian.destroy', $kajian->id),
                        'url_edit' => route('kajian.edit', $kajian->id),
                        'url_show' => route('kajian.show', $kajian->id),
                    ]);
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        } else {
            return response()->json(['message' => 'This resource accept ajax only'], 500);
        }
    }

    /**
     * Create a new instance kajian
     */
    public function newKajian() {
        return new $this->model;
    }

    /**
     * Get kajian based on id
     * 
     * @param int $id
     */
    public function getKajianById($id) {
        return $this->model->findOrFail($id);
    }

    /**
     * Store to the database
     * 
     * @param KajianRequest $request
     * @param int $id
     */
    public function storeToDB(KajianRequest $request, $id = null) {
        DB::beginTransaction();

        try {
            $kajian = $id !== null ? $this->getKajianById($id) : $this->newKajian();
            $kajian->image = Common::uploadImage($request, 'image', 'kajians', $kajian);
            $kajian->title = $request->input('title');
            $kajian->description = $request->input('description');
            $kajian->speaker = $request->input('speaker');
            $kajian->date = $request->input('date');
            $kajian->time = $request->input('time');
            $kajian->location = $request->input('location');
            $kajian->created_by = Auth::user()->name;

            $kajian->save();

            DB::commit();

            if ($id !== null) {
                return redirect()->route('kajian.index')->with('success', 'Kajian berhasil diubah!');
            } else {
                return redirect()->route('kajian.index')->with('success', 'Kajian berhasil ditambah!');
            }
        } catch (\Exception $e) {
            DB::rollback();

            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Delete kajian from the database
     * 
     * @param int $id
     */
    public function deleteKajian($id) {
        DB::beginTransaction();

        try {
            $kajian = $this->getKajianById($id)->delete();

            DB::commit();

            return $this->success('Kajian berhasil dihapus', 500);
        } catch(\Exception $e) {
            DB::rollback();

            return $this->error($e->getMessage(), $e->getCode());
        }
    }

}