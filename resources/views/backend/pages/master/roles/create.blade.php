{!! Form::model($role, [
  'route' => $role->exists ? ['role.update', $role->id] : 'role.store',
  'method' => $role->exists ? 'PUT' : 'POST'
]) !!}

  <div class="form-group">
    {!! Form::label('name', 'Nama') !!}
    {!! Form::text('name', $role->exists ? $role->name : null, ['class' => 'form-control']) !!}
  </div>

{!! Form::close() !!}