<footer class="main-footer">
  <div class="footer-left">
    &copy; <script>document.writeln(new Date().getFullYear())</script>
  </div>
  <div class="footer-right">
    2.3.0
  </div>
</footer>