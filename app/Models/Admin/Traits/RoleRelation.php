<?php

namespace App\Models\Admin\Traits;

use App\User;

trait RoleRelation {

    public function user() {
        return $this->hasMany(User::class);
    }

}