<?php

namespace App\Interfaces\Admin\General;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\KajianRequest;

interface KajianInterface {

    /**
     * Request kajian using ajax
     * 
     * @param Request $request
     */
    public function requestKajian(Request $request);

    /**
     * Create a new instance kajian
     */
    public function newKajian();

    /**
     * Get kajian based on id
     * 
     * @param int $id
     */
    public function getKajianById($id);

    /**
     * Store to the database
     * 
     * @param KajianRequest $request
     * @param int $id
     */
    public function storeToDB(KajianRequest $request, $id = null);

    /**
     * Delete kajian from the database
     * 
     * @param int $id
     */
    public function deleteKajian($id);

}